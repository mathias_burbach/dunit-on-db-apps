unit FEmployeeFrame;

interface

uses
  System.Classes,
  Data.DB,
  Vcl.ComCtrls,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.DBCtrls,
  Vcl.StdCtrls,
  Vcl.Buttons,
  Vcl.ExtCtrls,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Mask,
  DEmployee,
  FBaseDBFrame;

type
  TfraEmployee = class(TfraBaseDB)
    edtFilterByLastName: TEdit;
    lblFilterByLastName: TLabel;
    edtFilterByFirstName: TEdit;
    lblFilterByFirstName: TLabel;
    dbeEmployeeID: TDBEdit;
    lblEmployeeID: TLabel;
    lblFirstName: TLabel;
    dbeFirstName: TDBEdit;
    dbeLastName: TDBEdit;
    lblLastName: TLabel;
    tabWork: TTabSheet;
    dscWork: TDataSource;
    pnlGrid: TPanel;
    Splitter2: TSplitter;
    ScrollBox1: TScrollBox;
    pnlWorkNavigator: TPanel;
    grdWork: TDBGrid;
    navWork: TDBNavigator;
    dbeStartDate: TDBEdit;
    dbeEndDate: TDBEdit;
    dbeHours: TDBEdit;
    lblProject: TLabel;
    lblStartDate: TLabel;
    lblEndDate: TLabel;
    lblHours: TLabel;
    dscProjectLU: TDataSource;
    lupProject: TDBLookupComboBox;
    procedure btnSearchDBClick(Sender: TObject);
    procedure pgcRecordViewChange(Sender: TObject);
  private
    { Private declarations }
    dmoEmployee: TdmoEmployee;
  public
    { Public declarations }
    procedure InitialiseDataAccess; override;
    procedure SetFocus; override;
  end;

var
  fraEmployee: TfraEmployee;

implementation

{$R *.dfm}

{ TfraEmployee }

procedure TfraEmployee.btnSearchDBClick(Sender: TObject);
begin
  inherited;
  dmoEmployee.Open(edtFilterByLastName.Text, edtFilterByFirstName.Text);
end;

procedure TfraEmployee.InitialiseDataAccess;
begin
  inherited;
  pgcRecordView.ActivePage := tabRecordView;
  dmoEmployee := TdmoEmployee.Create(Self);
  dscMain.DataSet := dmoEmployee.qryEmployee;
  dscWork.DataSet := dmoEmployee.qryWork;
  dscProjectLU.DataSet := dmoEmployee.qryProjectLU;
  btnSearchDB.Click;
end;

procedure TfraEmployee.pgcRecordViewChange(Sender: TObject);
begin
  inherited;
  dmoEmployee.WorkActive := (pgcRecordView.ActivePage = tabWork);
end;

procedure TfraEmployee.SetFocus;
begin
  inherited;
  edtFilterByFirstName.SetFocus;
end;

end.
