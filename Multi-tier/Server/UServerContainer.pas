unit UServerContainer;

interface

uses System.SysUtils, System.Classes,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSServer, Datasnap.DSCommonServer,
  Datasnap.DSAuth, IPPeerServer;

type
  TsrvContainer = class(TDataModule)
    DSServer: TDSServer;
    tcpTransport: TDSTCPServerTransport;
    sclEmployee: TDSServerClass;
    sclLookup: TDSServerClass;
    procedure sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                  var PersistentClass: TPersistentClass);
    procedure sclLookupGetClass(DSServerClass: TDSServerClass;
                                var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
  end;

var
  srvContainer: TsrvContainer;

implementation


{$R *.dfm}

uses
  SEmployee,
  SLookup;

procedure TsrvContainer.sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                            var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmEmployee;
end;


procedure TsrvContainer.sclLookupGetClass(DSServerClass: TDSServerClass;
                                          var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmLookup;
end;

end.

