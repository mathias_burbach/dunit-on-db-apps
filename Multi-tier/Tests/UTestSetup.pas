unit UTestSetup;

interface

uses
  TestFrameWork,
  TestExtensions;

type
  TWorkLogTestSetup = class(TTestSetup)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  end;

implementation

{ TWorkLogTestSetup }

uses
  DTestHelper,
  UTestSEmployee,
  UTestClientSetup,
  UTestDEmployee,
  UTestDLookup,
  UTestFEmployee,
  UTestFLookup;

procedure TWorkLogTestSetup.SetUp;
begin
  inherited;
  dmoTestHelper := TdmoTestHelper.Create(nil);
  dmoTestHelper.CreateFixture;
end;

procedure TWorkLogTestSetup.TearDown;
begin
  dmoTestHelper.Free;
  inherited;
end;

initialization
  RegisterTest(
    TWorkLogTestSetup.Create(
      TTestSuite.Create('WorkLog Multi-Tier Tests',
        [TTestSuite.Create('App Server Tests',
          [TTestsvmEmployee.Suite]),
         TClientTestSetup.Create(
           TTestSuite.Create('Thin Client Tests',
             [TTestdmoEmployee.Suite,
              TTestdmoLookup.Suite,
              TTestfraEmployee.Suite,
              TTestfraLookup.Suite]))])));
end.
