object svmBase: TsvmBase
  Height = 205
  Width = 215
  object conFB: TFDConnection
    Params.Strings = (
      'Database=WorkLog'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvRefreshDelete]
    UpdateOptions.RefreshDelete = False
    ConnectedStoredUsage = [auDesignTime]
    Connected = True
    LoginPrompt = False
    Left = 56
    Top = 40
  end
  object qryGetNextPKValue: TFDQuery
    Connection = conFB
    SQL.Strings = (
      'Select Gen_ID(GenPersonID, 1)'#11
      'From RDB$Database')
    Left = 56
    Top = 112
  end
end
