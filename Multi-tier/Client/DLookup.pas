unit DLookup;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.DBClient,
  Datasnap.DSConnect,
  DBase;

type
  TdmoLookup = class(TdmoBase)
    dpcLookup: TDSProviderConnection;
    cdsLookup: TClientDataSet;
    procedure cdsLookupNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FTableName, FName: string;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent;
                       const TableName: string); reintroduce;
    procedure Open(const Name: string);
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  System.SysUtils,
  DMain;

{$R *.dfm}

{ TdmoLookup }

procedure TdmoLookup.cdsLookupNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsLookup.Fields[0].ReadOnly := False;
  try
    cdsLookup.Fields[0].AsInteger := GetNextTemporaryPK;
  finally
    cdsLookup.Fields[0].ReadOnly := True;
  end;
end;

constructor TdmoLookup.Create(AOwner: TComponent; const TableName: string);
begin
  inherited Create(AOwner);
  MainCDS := cdsLookup;
  FTableName := TableName;
  FName := '???';
end;

procedure TdmoLookup.Open(const Name: string);
begin
  if FName <> Name then
  begin
    cdsLookup.DisableControls;
    try
      cdsLookup.Close;
      try
        cdsLookup.ProviderName := 'dsp' + FTableName;
        cdsLookup.FetchParams;
        if Name = EmptyStr then
          cdsLookup.Params.ParamByName('Name').AsString := Name
        else
          cdsLookup.Params.ParamByName('Name').AsString := Name + '%';
      finally
        cdsLookup.Open;
      end;
      FName := Name;
      cdsLookup.Fields[0].DisplayLabel := 'ID';
      cdsLookup.Fields[0].ReadOnly := True;
      cdsLookup.Fields[1].DisplayLabel := 'Name';
      cdsLookup.Fields[2].Visible := False;
    finally
      cdsLookup.EnableControls;
    end;
  end;
end;

end.
