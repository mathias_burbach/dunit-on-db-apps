program WorkLogTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  DEmployee in '..\Client\DEmployee.pas' {dmoEmployee: TDataModule},
  DMain in '..\Client\DMain.pas' {dmoMain: TDataModule},
  DBase in '..\Client\DBase.pas' {dmoBase: TDataModule},
  UTestSetup in 'UTestSetup.pas',
  UTestDEmployee in 'UTestDEmployee.pas',
  UTestBase in 'UTestBase.pas',
  DTestHelper in 'DTestHelper.pas' {dmoTestHelper: TDataModule},
  DLookup in '..\Client\DLookup.pas' {dmoLookup: TDataModule},
  UTestDLookup in 'UTestDLookup.pas',
  FBaseFrame in '..\Client\FBaseFrame.pas' {fraBase: TFrame},
  FBaseDBFrame in '..\Client\FBaseDBFrame.pas' {fraBaseDB: TFrame},
  FEmployeeFrame in '..\Client\FEmployeeFrame.pas' {fraEmployee: TFrame},
  FLookupFrame in '..\Client\FLookupFrame.pas' {fraLookup: TFrame},
  FTestGUI in 'FTestGUI.pas' {frmTestGUI},
  UTestBaseGUI in 'UTestBaseGUI.pas',
  UTestFEmployee in 'UTestFEmployee.pas',
  UTestFLookup in 'UTestFLookup.pas';

{R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.

