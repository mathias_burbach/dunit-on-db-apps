unit UTestSEmployee;

interface

uses
  DUnitX.TestFramework,
  Datasnap.DBClient,
  UTestBase,
  SEmployee;

type
  [Category('ServerTests')]
  TTestsvmEmployee = class(TBaseTestCase)
  private
    svmEmployee: TsvmEmployee;
    cdsEmployee: TClientDataSet;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestOpen;
    [Test]
    procedure TestAdd;
    [Test]
    procedure TestEdit;
    [Test]
    procedure TestDelete;
    [Test]
    procedure TestOpenWork;
    [Test]
    procedure TestAddWork;
    [Test]
    procedure TestEditWork;
    [Test]
    procedure TestDeleteWork;
    [Test]
    procedure TestExportWorkingHoursToCSV;
  end;

implementation

{ TTestsvmEmployee }

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  DTestHelper;

procedure TTestsvmEmployee.Setup;
begin
  inherited Setup;
  svmEmployee := TsvmEmployee.Create(nil);
  cdsEmployee := TClientDataSet.Create(nil);
  cdsEmployee.SetProvider(svmEmployee.dspEmployee);
  cdsEmployee.FetchParams;
end;

procedure TTestsvmEmployee.TearDown;
begin
  cdsEmployee.Free;
  svmEmployee.Free;
  inherited TearDown;
end;

procedure TTestsvmEmployee.TestAdd;
const
  cLastName = 'Burbach';
  cFirstName = 'Mathias';
  cNextEmployeeID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cFirstName;
  cdsEmployee.Open;
  Assert.AreEqual(0, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsEmployee.Append;
  cdsEmployee.FieldByName('LastName').AsString := cLastName;
  cdsEmployee.FieldByName('FirstName').AsString := cFirstName;
  cdsEmployee.Post;
  cdsEmployee.ApplyUpdates(0);
  Assert.AreEqual(0, cdsEmployee.FieldByName('UpdNo').AsInteger, 'UpdNo');
  Assert.AreEqual(cNextEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cNextEmployeeID]));
  Assert.AreEqual(cLastName, DBValue, 'LastName');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'FirstName', Format('EmployeeID=%d', [cNextEmployeeID]));
  Assert.AreEqual(cFirstName, DBValue, 'FirstName');
end;

procedure TTestsvmEmployee.TestAddWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cEmployeeID = 1;
  cProjectID = 3;
  cNextProjectWorkID = 4;
  cHours = 25.0;
var
  cdsProjectWork: TClientDataSet;
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    Assert.AreEqual(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
    cdsProjectWork.Append;
    cdsProjectWork.FieldByName('StartDate').AsDateTime := Date;
    cdsProjectWork.FieldByName('ProjectID').AsInteger := cProjectID;
    cdsProjectWork.FieldByName('Hours').AsFloat := cHours;
    cdsProjectWork.Post;
    cdsEmployee.ApplyUpdates(0);
    Assert.AreEqual<Int64>(0, cdsEmployee.ChangeCount, 'ChangeCount');
    cdsProjectWork.Last;
    Assert.AreEqual(cNextProjectWorkID, cdsProjectWork.FieldByName('ProjectWorkID').AsInteger, 'ProjectWorkID');
    Assert.AreEqual(0, cdsProjectWork.FieldByName('UpdNo').AsInteger, 'UpdNo');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'StartDate', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
    Assert.AreEqual(DateToStr(Date), DBValue, 'StartDate');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
    Assert.AreEqual(FloatToStr(cHours), DBValue, 'Hours');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProjectID', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
    Assert.AreEqual(IntToStr(cProjectID), DBValue, 'ProjectID');
  finally
    cdsProjectWork.Free;
  end;
end;

procedure TTestsvmEmployee.TestDelete;
const
  cLastName = 'Smith';
  cAnyValue = '%';
  cEmployeeID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  cdsEmployee.Delete;
  cdsEmployee.ApplyUpdates(0);
  DBValue := dmoTestHelper.GetDBValue('Employee', 'Count(1)', Format('EmployeeID=%d', [cEmployeeID]));
  Assert.AreEqual('0', DBValue, 'Count');
end;

procedure TTestsvmEmployee.TestDeleteWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cEmployeeID = 1;
  cProjectWorkID = 2;
var
  cdsProjectWork: TClientDataSet;
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    Assert.AreEqual(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
    cdsProjectWork.Last;
    Assert.AreEqual(cProjectWorkID, cdsProjectWork.FieldByName('ProjectWorkID').AsInteger, 'ProjectWorkID');
    cdsProjectWork.Delete;
    cdsEmployee.ApplyUpdates(0);
    Assert.AreEqual<Int64>(0, cdsEmployee.ChangeCount, 'ChangeCount');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Count(1)', Format('ProjectWorkID=%d', [cProjectWorkID]));
    Assert.AreEqual('0', DBValue, 'Count');
  finally
    cdsProjectWork.Free;
  end;
end;

procedure TTestsvmEmployee.TestEdit;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cChangedName = 'NewLastName';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  cdsEmployee.Edit;
  cdsEmployee.FieldByName('LastName').AsString := cChangedName;
  cdsEmployee.Post;
  cdsEmployee.ApplyUpdates(0);
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cEmployeeID]));
  Assert.AreEqual(cChangedName, DBValue, 'LastName');
end;

procedure TTestsvmEmployee.TestEditWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  cdsProjectWork: TClientDataSet;
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    Assert.AreEqual(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
    cdsProjectWork.Last;
    Assert.AreEqual(cProjectWorkID, cdsProjectWork.FieldByName('ProjectWorkID').AsInteger, 'ProjectWorkID');
    cdsProjectWork.Edit;
    cdsProjectWork.FieldByName('EndDate').AsDateTime := Date;
    cdsProjectWork.FieldByName('Hours').AsFloat := cHours;
    cdsProjectWork.Post;
    cdsEmployee.ApplyUpdates(0);
    Assert.AreEqual<Int64>(0, cdsEmployee.ChangeCount, 'ChangeCount');
    cdsProjectWork.Last;
    Assert.AreEqual(1, cdsProjectWork.FieldByName('UpdNo').AsInteger, 'UpdNo');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'EndDate', Format('ProjectWorkID=%d', [cProjectWorkID]));
    Assert.AreEqual(DateToStr(Date), DBValue, 'EndDate');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cProjectWorkID]));
    Assert.AreEqual(FloatToStr(cHours), DBValue, 'Hours');
  finally
    cdsProjectWork.Free;
  end;
end;

procedure TTestsvmEmployee.TestExportWorkingHoursToCSV;
const
  caCSV: array[0..1] of string =
    ('EMPLOYEEID,FIRSTNAME,LASTNAME,PROJECT,STARTDATE,ENDDATE,HOURS',
     '1,"Joe","Bloggs","Project A",14/07/2014,18/07/2014,40');
var
  DBValue: string;
  stlCSV: TStringList;
begin
  RunCreateFixtureAtTearDown := True;
  stlCSV := TStringList.Create;
  try
    stlCSV.Text := svmEmployee.ExportWorkingHoursToCSV;
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProcessedStmp', 'ProjectWorkID=1');
    Assert.IsTrue(DBValue <> EmptyStr, 'ProcessedStmp Is Not Null');
    Assert.AreEqual(2, stlCSV.Count, 'CSV.Count');
    Assert.AreEqual(caCSV[0], stlCSV[0], 'stlCSV[0]');
    Assert.AreEqual(caCSV[1], stlCSV[1], 'stlCSV[1]');
  finally
    stlCSV.Free;
  end;
end;

procedure TTestsvmEmployee.TestOpen;
const
  cLastName = 'B%';
  cAnyValue = '%';
begin
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsEmployee.Close;
  // closing the CDS causes the provider to be "lost"
  cdsEmployee.SetProvider(svmEmployee.dspEmployee);
  cdsEmployee.Params.ParamByName('LastName').AsString := cAnyValue;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(3, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
end;

procedure TTestsvmEmployee.TestOpenWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
var
  cdsProjectWork: TClientDataSet;
begin
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  Assert.AreEqual(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    Assert.AreEqual(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
  finally
    cdsProjectWork.Free;
  end;
end;

initialization
  TDUnitX.RegisterTestFixture(TTestsvmEmployee);
end.
