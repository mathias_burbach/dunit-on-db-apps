program WorkLogThinClient;

uses
  Vcl.Forms,
  DBase in 'DBase.pas' {dmoBase: TDataModule},
  DEmployee in 'DEmployee.pas' {dmoEmployee: TDataModule},
  DLookup in 'DLookup.pas' {dmoLookup: TDataModule},
  DMain in 'DMain.pas' {dmoMain: TDataModule},
  FBaseFrame in 'FBaseFrame.pas' {fraBase: TFrame},
  FMain in 'FMain.pas' {frmMain},
  FBaseDBFrame in 'FBaseDBFrame.pas' {fraBaseDB: TFrame},
  FLookupFrame in 'FLookupFrame.pas' {fraLookup: TFrame},
  FEmployeeFrame in 'FEmployeeFrame.pas' {fraEmployee: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmoMain, dmoMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
