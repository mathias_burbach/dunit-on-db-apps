unit UTestFLookup;

interface

uses
  UTestBaseGUI,
  FLookupFrame;

type
  TTestfraLookup = class(TBaseGUITestCase)
  private
    fraLookup: TfraLookup;
  protected
    procedure SetUp; override;
  published
    procedure TestOpen;
    procedure TestAdd;
    procedure TestEdit;
    procedure TestDelete;
  end;

implementation

uses
  System.SysUtils,
  Vcl.Controls,
  Vcl.DBCtrls,
  DTestHelper;

{ TTestfraAgency }

procedure TTestfraLookup.SetUp;
begin
  inherited;
  fraLookup := TfraLookup.Create(frmTestGUI, 'Project');
  fraLookup.Parent := frmTestGUI;
  fraLookup.Align := alClient;
  fraLookup.InitialiseDataAccess;
  fraLookup.SetFocus;
end;

procedure TTestfraLookup.TestAdd;
const
  cProject = 'Project D';
  cNextProjectID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  EnterText(cProject);
  Tab;
  Click;
  CheckEquals(0, fraLookup.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  SetFocus(fraLookup.dbeName);
  fraLookup.navMain.BtnClick(nbInsert);
  EnterText(cProject);
  fraLookup.navMain.BtnClick(nbPost);
  CheckEnabled(fraLookup.btnApplyUpdates);
  fraLookup.btnApplyUpdates.Click;
  CheckFalse(fraLookup.btnApplyUpdates.Enabled, 'btnApplyUpdates.Enabled');
  CheckEquals(cNextProjectID, fraLookup.dscMain.DataSet.Fields[0].AsInteger, 'ID');
  DBValue := dmoTestHelper.GetDBValue('Project', 'Name', Format('ProjectID=%d', [cNextProjectID]));
  CheckEquals(cProject, DBValue, 'Name');
end;

procedure TTestfraLookup.TestDelete;
const
  cProject = 'Project C';
  cProjectID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  EnterText(cProject);
  Tab;
  Click;
  CheckEquals(1, fraLookup.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  SetFocus(fraLookup.dbeName);
  CheckTrue(fraLookup.navMain.ConfirmDelete, 'ConfirmDelete');
  fraLookup.navMain.ConfirmDelete := False;
  fraLookup.navMain.BtnClick(nbDelete);
  CheckEnabled(fraLookup.btnApplyUpdates);
  fraLookup.btnApplyUpdates.Click;
  CheckFalse(fraLookup.btnApplyUpdates.Enabled, 'btnApplyUpdates.Enabled');
  DBValue := dmoTestHelper.GetDBValue('Project', 'Count(1)', Format('ProjectID=%d', [cProjectID]));
  CheckEquals('0', DBValue, 'Count');
end;

procedure TTestfraLookup.TestEdit;
const
  cProject = 'Project A';
  cChangedName = 'Project A1';
  cProjectID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  EnterText(cProject);
  Tab;
  Click;
  CheckEquals(1, fraLookup.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  SetFocus(fraLookup.dbeName);
  fraLookup.navMain.BtnClick(nbEdit);
  EnterText(cChangedName);
  fraLookup.navMain.BtnClick(nbPost);
  CheckEnabled(fraLookup.btnApplyUpdates);
  fraLookup.btnApplyUpdates.Click;
  CheckFalse(fraLookup.btnApplyUpdates.Enabled, 'btnApplyUpdates.Enabled');
  CheckEquals(1, fraLookup.dscMain.DataSet.Fields[2].AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('Project', 'Name', Format('ProjectID=%d', [cProjectID]));
  CheckEquals(cChangedName, DBValue, 'Name');
end;

procedure TTestfraLookup.TestOpen;
const
  cProject = 'Project';
  cNoProject = 'No Project';
begin
  EnterText(cProject);
  Tab;
  Click;
  CheckEquals(3, fraLookup.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  Tab(-1);
  fraLookup.edtFilterByName.SetFocus;
  EnterText(cNoProject);
  Tab;
  Click;
  CheckEquals(0, fraLookup.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
end;

end.
