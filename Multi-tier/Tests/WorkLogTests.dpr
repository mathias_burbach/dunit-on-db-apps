program WorkLogTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  UTestBase in 'UTestBase.pas',
  DTestHelper in 'DTestHelper.pas' {dmoTestHelper: TDataModule},
  UTestSetup in 'UTestSetup.pas',
  UTestSEmployee in 'UTestSEmployee.pas',
  SEmployee in '..\Server\SEmployee.pas' {svmEmployee: TDSServerModule},
  SBase in '..\Server\SBase.pas' {svmBase: TDSServerModule},
  UTestClientSetup in 'UTestClientSetup.pas',
  DMain in '..\Client\DMain.pas' {dmoMain: TDataModule},
  UTestDEmployee in 'UTestDEmployee.pas',
  DBase in '..\Client\DBase.pas' {dmoBase: TDataModule},
  DEmployee in '..\Client\DEmployee.pas' {dmoEmployee: TDataModule},
  DLookup in '..\Client\DLookup.pas' {dmoLookup: TDataModule},
  UTestDLookup in 'UTestDLookup.pas',
  FTestGUI in 'FTestGUI.pas' {frmTestGUI},
  UTestBaseGUI in 'UTestBaseGUI.pas',
  UTestFLookup in 'UTestFLookup.pas',
  FBaseFrame in '..\Client\FBaseFrame.pas' {fraBase: TFrame},
  FBaseDBFrame in '..\Client\FBaseDBFrame.pas' {fraBaseDB: TFrame},
  FLookupFrame in '..\Client\FLookupFrame.pas' {fraLookup: TFrame},
  FEmployeeFrame in '..\Client\FEmployeeFrame.pas' {fraEmployee: TFrame},
  UTestFEmployee in 'UTestFEmployee.pas';

{R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.

