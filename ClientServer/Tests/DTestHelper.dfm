object dmoTestHelper: TdmoTestHelper
  OnCreate = DataModuleCreate
  Height = 150
  Width = 372
  object conFB: TFDConnection
    Params.Strings = (
      'Database=WorkLog'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvRefreshDelete]
    UpdateOptions.RefreshDelete = False
    ConnectedStoredUsage = [auDesignTime]
    Connected = True
    LoginPrompt = False
    Left = 56
    Top = 40
  end
  object scrCreateFixture: TFDScript
    SQLScripts = <
      item
        Name = 'AllInOne'
        SQL.Strings = (
          'Delete From ProjectWork;'
          'Delete From Project;'
          'Delete From Employee;'
          ''
          
            'Insert Into Project (ProjectID, Name, UpdNo) Values (1, '#39'Project' +
            ' A'#39', 0);'
          
            'Insert Into Project (ProjectID, Name, UpdNo) Values (2, '#39'Project' +
            ' B'#39', 0);'
          
            'Insert Into Project (ProjectID, Name, UpdNo) Values (3, '#39'Project' +
            ' C'#39', 0);'
          ''
          
            'Insert Into Employee (EmployeeID, FirstName, LastName, UpdNo) Va' +
            'lues (1, '#39'Joe'#39', '#39'Bloggs'#39', 0);'
          
            'Insert Into Employee (EmployeeID, FirstName, LastName, UpdNo) Va' +
            'lues (2, '#39'Peter'#39', '#39'Miller'#39', 0);'
          
            'Insert Into Employee (EmployeeID, FirstName, LastName, UpdNo) Va' +
            'lues (3, '#39'Tara'#39', '#39'Smith'#39', 0);'
          ''
          
            'Insert Into ProjectWork (ProjectWorkID, EmployeeID, ProjectID, S' +
            'tartDate, EndDate, Hours, UpdNo) Values (1, 1, 1, '#39'2014-07-14'#39', ' +
            #39'2014-07-18'#39', 40.0, 0);'
          
            'Insert Into ProjectWork (ProjectWorkID, EmployeeID, ProjectID, S' +
            'tartDate, EndDate, Hours, UpdNo) Values (2, 1, 2, '#39'2014-07-20'#39', ' +
            'Null, 15.0, 0);'
          
            'Insert Into ProjectWork (ProjectWorkID, EmployeeID, ProjectID, S' +
            'tartDate, EndDate, Hours, UpdNo) Values (3, 2, 2, '#39'2014-07-20'#39', ' +
            'Null, 8.0, 0);'
          ''
          'Set Generator GenProjectID To 3;'
          'Set Generator GenEmployeeID To 3;'
          'Set Generator GenProjectWorkID To 3;')
      end>
    Connection = conFB
    Params = <>
    Macros = <>
    Left = 152
    Top = 40
  end
  object qryTask: TFDQuery
    Connection = conFB
    Left = 256
    Top = 40
  end
end
