inherited dmoEmployee: TdmoEmployee
  OnCreate = DataModuleCreate
  Height = 268
  Width = 433
  object qryEmployee: TFDQuery
    BeforePost = qryBeforePost
    Connection = dmoMain.conFB
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate, uvUpdateMode, uvGeneratorName]
    UpdateOptions.UpdateMode = upWhereChanged
    UpdateOptions.GeneratorName = 'GENEMPLOYEEID'
    UpdateOptions.KeyFields = 'EMPLOYEEID'
    SQL.Strings = (
      'Select *'
      'From Employee'
      'Where LastName Like :LastName'
      '  And FirstName Like :FirstName'
      'Order By LastName, FirstName')
    Left = 64
    Top = 48
    ParamData = <
      item
        Name = 'LASTNAME'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = Null
      end
      item
        Name = 'FIRSTNAME'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end>
    object qryEmployeeEMPLOYEEID: TIntegerField
      AutoGenerateValue = arAutoInc
      DisplayLabel = 'ID'
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      ReadOnly = True
    end
    object qryEmployeeFIRSTNAME: TStringField
      DisplayLabel = 'First Name'
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
    end
    object qryEmployeeLASTNAME: TStringField
      DisplayLabel = 'Last Name'
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
    end
    object qryEmployeeUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
      Required = True
      Visible = False
    end
  end
  object dstEmployee: TDataSource
    DataSet = qryEmployee
    Left = 152
    Top = 48
  end
  object qryWork: TFDQuery
    BeforePost = qryBeforePost
    OnNewRecord = qryWorkNewRecord
    MasterSource = dstEmployee
    MasterFields = 'EMPLOYEEID'
    Connection = dmoMain.conFB
    UpdateOptions.AssignedValues = [uvUpdateMode, uvGeneratorName]
    UpdateOptions.UpdateMode = upWhereChanged
    UpdateOptions.GeneratorName = 'GENPROJECTWORKID'
    UpdateOptions.KeyFields = 'PROJECTWORKID'
    SQL.Strings = (
      'Select *'
      'From ProjectWork'
      'Where EmployeeID = :EmployeeID'
      'Order By StartDate')
    Left = 232
    Top = 48
    ParamData = <
      item
        Name = 'EMPLOYEEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryWorkPROJECTWORKID: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'PROJECTWORKID'
      Origin = 'PROJECTWORKID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      ReadOnly = True
      Visible = False
    end
    object qryWorkEMPLOYEEID: TIntegerField
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      Required = True
      Visible = False
    end
    object qryWorkPROJECTID: TIntegerField
      FieldName = 'PROJECTID'
      Origin = 'PROJECTID'
      Required = True
      Visible = False
    end
    object qryWorkProject: TStringField
      DisplayWidth = 25
      FieldKind = fkLookup
      FieldName = 'Project'
      LookupDataSet = qryProjectLU
      LookupKeyFields = 'PROJECTID'
      LookupResultField = 'NAME'
      KeyFields = 'PROJECTID'
      Size = 35
      Lookup = True
    end
    object qryWorkSTARTDATE: TDateField
      DisplayLabel = 'Start Date'
      FieldName = 'STARTDATE'
      Origin = 'STARTDATE'
      Required = True
    end
    object qryWorkENDDATE: TDateField
      DisplayLabel = 'End Date'
      FieldName = 'ENDDATE'
      Origin = 'ENDDATE'
    end
    object qryWorkHOURS: TFloatField
      DisplayLabel = 'Hours'
      FieldName = 'HOURS'
      Origin = 'HOURS'
    end
    object qryWorkPROCESSEDSTMP: TSQLTimeStampField
      FieldName = 'PROCESSEDSTMP'
      Origin = 'PROCESSEDSTMP'
      Visible = False
    end
    object qryWorkUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
      Required = True
      Visible = False
    end
  end
  object qryProjectLU: TFDQuery
    Connection = dmoMain.conFB
    SQL.Strings = (
      'Select Name, ProjectID'
      'From Project'
      'Order By Name')
    Left = 232
    Top = 120
  end
  object cmdMarkProcessed: TFDCommand
    Connection = dmoMain.conFB
    Left = 152
    Top = 200
  end
  object qryExport: TFDQuery
    Connection = dmoMain.conFB
    FetchOptions.AssignedValues = [evMode, evUnidirectional]
    FetchOptions.Mode = fmAll
    FetchOptions.Unidirectional = True
    SQL.Strings = (
      'Select e.EmployeeID, e.FirstName, e.LastName, p.Name Project,'
      '  w.StartDate, w.EndDate, w.Hours, w.ProjectWorkID'
      'From ProjectWork w'
      '  Join Employee e'
      '    On w.EmployeeID = e.EmployeeID'
      '  Join Project p'
      '    On w.ProjectID = p.ProjectID'
      'Where w.EndDate Is Not Null'
      '  And w.ProcessedStmp Is Null'
      'Order By w.StartDate')
    Left = 64
    Top = 200
    object qryExportEMPLOYEEID: TIntegerField
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      Required = True
    end
    object qryExportFIRSTNAME: TStringField
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
    end
    object qryExportLASTNAME: TStringField
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
    end
    object qryExportPROJECT: TStringField
      FieldName = 'PROJECT'
      Origin = 'PROJECT'
      Required = True
    end
    object qryExportSTARTDATE: TDateField
      FieldName = 'STARTDATE'
      Origin = 'STARTDATE'
      Required = True
    end
    object qryExportENDDATE: TDateField
      FieldName = 'ENDDATE'
      Origin = 'ENDDATE'
    end
    object qryExportHOURS: TFloatField
      FieldName = 'HOURS'
      Origin = 'HOURS'
    end
    object qryExportPROJECTWORKID: TIntegerField
      FieldName = 'PROJECTWORKID'
      Origin = 'PROJECTWORKID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
end
