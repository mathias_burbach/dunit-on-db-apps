unit DEmployee;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Stan.Async,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  DBase;

type
  TdmoEmployee = class(TdmoBase)
    qryEmployee: TFDQuery;
    qryEmployeeEMPLOYEEID: TIntegerField;
    qryEmployeeFIRSTNAME: TStringField;
    qryEmployeeLASTNAME: TStringField;
    qryEmployeeUPDNO: TIntegerField;
    dstEmployee: TDataSource;
    qryWork: TFDQuery;
    qryWorkPROJECTWORKID: TIntegerField;
    qryWorkEMPLOYEEID: TIntegerField;
    qryWorkPROJECTID: TIntegerField;
    qryWorkSTARTDATE: TDateField;
    qryWorkENDDATE: TDateField;
    qryWorkHOURS: TFloatField;
    qryWorkPROCESSEDSTMP: TSQLTimeStampField;
    qryWorkUPDNO: TIntegerField;
    qryProjectLU: TFDQuery;
    qryWorkProject: TStringField;
    cmdMarkProcessed: TFDCommand;
    qryExport: TFDQuery;
    qryExportEMPLOYEEID: TIntegerField;
    qryExportFIRSTNAME: TStringField;
    qryExportLASTNAME: TStringField;
    qryExportPROJECT: TStringField;
    qryExportSTARTDATE: TDateField;
    qryExportENDDATE: TDateField;
    qryExportHOURS: TFloatField;
    qryExportPROJECTWORKID: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure qryBeforePost(DataSet: TDataSet);
    procedure qryWorkNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FLastName, FFirstName: string;
    function GetWorkActive: Boolean;
    procedure SetWorkActive(const Value: Boolean);
    procedure MarkWorkAsExported(const ProjectWorkIDs: string);
  public
    { Public declarations }
    procedure Open(const LastName, FirstName: string);
    procedure ExportWorkingHoursToCSV(const FileName: string);
    property WorkActive: Boolean read GetWorkActive write SetWorkActive;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  DMain;

{$R *.dfm}

{ TdmoEmployee }

procedure TdmoEmployee.DataModuleCreate(Sender: TObject);
begin
  inherited;
  FLastName := '???';
  FFirstName := '???';
end;

procedure TdmoEmployee.ExportWorkingHoursToCSV(const FileName: string);
  function BuildCSVHeader: string;
  var
    i: Integer;
  begin
    Result := EmptyStr;
    for i := 0 to qryExport.FieldCount-2 do // do not export ProjectWorkID
      Result := Result + Format('%s,', [qryExport.Fields[i].FieldName]);
    Result := Copy(Result, 1, Length(Result)-1);
  end;
  function BuildCSVRow: string;
  var
    i: Integer;
  begin
    Result := EmptyStr;
    for i := 0 to qryExport.FieldCount-2 do // do not export ProjectWorkID
      if qryExport.Fields[i].DataType = ftString then
        Result := Result + Format('"%s",', [qryExport.Fields[i].AsString])
      else
        Result := Result + Format('%s,', [qryExport.Fields[i].AsString]);
    Result := Copy(Result, 1, Length(Result)-1);
  end;
var
  stlCSV: TStringList;
  sProjectWorkIDs: string;
begin
  qryExport.Open;
  try
    if not qryExport.Eof then
    begin
      stlCSV := TStringList.Create;
      try
        sProjectWorkIDs := EmptyStr;
        stlCSV.Add(BuildCSVHeader);
        while not qryExport.Eof do
        begin
          sProjectWorkIDs := sProjectWorkIDs +
                             Format('%d,', [qryExportPROJECTWORKID.AsInteger]);
          stlCSV.Add(BuildCSVRow);
          qryExport.Next;
        end;
        stlCSV.SaveToFile(FileName);
        sProjectWorkIDs := Copy(sProjectWorkIDs, 1, Length(sProjectWorkIDs)-1);
        MarkWorkAsExported(sProjectWorkIDs);
      finally
        stlCSV.Free;
      end;
    end;
  finally
    qryExport.Close;
  end;
end;

function TdmoEmployee.GetWorkActive: Boolean;
begin
  Result := qryWork.Active;
end;

procedure TdmoEmployee.MarkWorkAsExported(const ProjectWorkIDs: string);
begin
  cmdMarkProcessed.CommandText.Clear;
  cmdMarkProcessed.CommandText.Add('Update ProjectWork');
  cmdMarkProcessed.CommandText.Add('Set ProcessedStmp = Current_Timestamp');
  cmdMarkProcessed.CommandText.Add(Format('Where ProjectWorkID In (%s)', [ProjectWorkIDs]));
  cmdMarkProcessed.Execute;
end;

procedure TdmoEmployee.Open(const LastName, FirstName: string);
begin
  if (FLastName <> LastName) Or (FFirstName <> FirstName) then
  begin
    qryEmployee.DisableControls;
    try
      qryEmployee.Close;
      try
        if (LastName = EmptyStr) and (FirstName = EmptyStr) then
        begin
          qryEmployee.ParamByName('LastName').AsString := LastName;
          qryEmployee.ParamByName('FirstName').AsString := FirstName;
        end
        else
        begin
          qryEmployee.ParamByName('LastName').AsString := LastName + '%';
          qryEmployee.ParamByName('FirstName').AsString := FirstName + '%';
        end;
      finally
        qryEmployee.Open;
      end;
      FLastName := LastName;
      FFirstName := FirstName;
    finally
      qryEmployee.EnableControls;
    end;
  end;
end;

procedure TdmoEmployee.qryBeforePost(DataSet: TDataSet);
begin
  inherited;
  DoBeforePost(DataSet);
end;

procedure TdmoEmployee.qryWorkNewRecord(DataSet: TDataSet);
begin
  inherited;
  qryWorkSTARTDATE.AsDateTime := Date;
end;

procedure TdmoEmployee.SetWorkActive(const Value: Boolean);
begin
  qryWork.Active := Value;
end;

end.
