unit FLookupFrame;

interface

uses
  System.Classes,
  Data.DB,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.ComCtrls,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.DBCtrls,
  Vcl.StdCtrls,
  Vcl.Buttons,
  Vcl.ExtCtrls,
  Vcl.Mask,
  FBaseDBFrame,
  DLookup;

type
  TfraLookup = class(TfraBaseDB)
    edtFilterByName: TEdit;
    lblFilterByName: TLabel;
    dbeID: TDBEdit;
    lblID: TLabel;
    dbeName: TDBEdit;
    lblName: TLabel;
    procedure btnSearchDBClick(Sender: TObject);
  private
    { Private declarations }
    FTableName: string;
    dmoLookup: TdmoLookup;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent;
                       const TableName: string); reintroduce;
    procedure InitialiseDataAccess; override;
    procedure SetFocus; override;
  end;

var
  fraLookup: TfraLookup;

implementation

{$R *.dfm}

{ TfraLookup }

procedure TfraLookup.btnSearchDBClick(Sender: TObject);
begin
  inherited;
  dmoLookup.Open(edtFilterByName.Text);
end;

constructor TfraLookup.Create(AOwner: TComponent; const TableName: string);
begin
  inherited Create(AOwner);
  FTableName := TableName;
end;

procedure TfraLookup.InitialiseDataAccess;
begin
  inherited;
  dmoLookup := TdmoLookup.Create(Self, FTableName);
  dscMain.DataSet := dmoLookup.qryLookup;
  dbeID.DataField := FTableName + 'ID';
  btnSearchDB.Click;
end;

procedure TfraLookup.SetFocus;
begin
  inherited;
  edtFilterByName.SetFocus;
end;

end.
