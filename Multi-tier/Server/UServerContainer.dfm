object srvContainer: TsrvContainer
  OldCreateOrder = False
  Height = 158
  Width = 365
  object DSServer: TDSServer
    Left = 96
    Top = 11
  end
  object tcpTransport: TDSTCPServerTransport
    Server = DSServer
    Filters = <>
    Left = 96
    Top = 73
  end
  object sclEmployee: TDSServerClass
    OnGetClass = sclEmployeeGetClass
    Server = DSServer
    LifeCycle = 'Invocation'
    Left = 200
    Top = 11
  end
  object sclLookup: TDSServerClass
    OnGetClass = sclLookupGetClass
    Server = DSServer
    LifeCycle = 'Invocation'
    Left = 296
    Top = 11
  end
end
