unit UTestBase;

interface

type
  TBaseTestCase = class(TObject)
  private
    FRunCreateFixtureAtTearDown: Boolean;
    procedure SetRunCreateFixtureAtTearDown(const Value: Boolean);
  public
    procedure SetupFixture;
    procedure TearDownFixture;
    procedure SetUp;
    procedure TearDown;
    property RunCreateFixtureAtTearDown: Boolean read FRunCreateFixtureAtTearDown write SetRunCreateFixtureAtTearDown;
  end;

implementation

{ TBaseTestCase }

uses
  DTestHelper,
  DMain;

procedure TBaseTestCase.SetRunCreateFixtureAtTearDown(const Value: Boolean);
begin
  FRunCreateFixtureAtTearDown := Value;
end;

procedure TBaseTestCase.SetUp;
begin
  FRunCreateFixtureAtTearDown := False;
end;

procedure TBaseTestCase.SetupFixture;
begin
  dmoMain := TdmoMain.Create(nil);
end;

procedure TBaseTestCase.TearDown;
begin
  if FRunCreateFixtureAtTearDown then
  begin
    dmoTestHelper.CreateFixture;
    FRunCreateFixtureAtTearDown := False;
  end;
end;

procedure TBaseTestCase.TearDownFixture;
begin
  dmoMain.Free;
end;

end.
