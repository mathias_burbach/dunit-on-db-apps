inherited fraEmployee: TfraEmployee
  Width = 790
  Height = 433
  ExplicitWidth = 790
  ExplicitHeight = 433
  inherited Splitter1: TSplitter
    Left = 373
    Height = 351
    ExplicitLeft = 349
    ExplicitHeight = 351
  end
  inherited pnlFilter: TPanel
    Width = 790
    ExplicitWidth = 790
    DesignSize = (
      790
      41)
    object lblFilterByLastName: TLabel [0]
      Left = 504
      Top = 14
      Width = 50
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Last Name'
      FocusControl = edtFilterByLastName
    end
    object lblFilterByFirstName: TLabel [1]
      Left = 328
      Top = 14
      Width = 51
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'First Name'
      FocusControl = edtFilterByFirstName
    end
    object edtFilterByLastName: TEdit [2]
      Left = 568
      Top = 11
      Width = 97
      Height = 21
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object edtFilterByFirstName: TEdit [3]
      Left = 392
      Top = 11
      Width = 97
      Height = 21
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    inherited btnSearchDB: TBitBtn
      Left = 676
      TabOrder = 2
      OnClick = btnSearchDBClick
      ExplicitLeft = 676
    end
  end
  inherited pnlNavigator: TPanel
    Top = 392
    Width = 790
    ExplicitTop = 392
    ExplicitWidth = 790
    inherited navMain: TDBNavigator
      Hints.Strings = ()
    end
  end
  inherited grdMain: TDBGrid
    Width = 373
    Height = 351
  end
  inherited pgcRecordView: TPageControl
    Left = 376
    Width = 414
    Height = 351
    ExplicitLeft = 376
    ExplicitWidth = 414
    ExplicitHeight = 351
    inherited tabRecordView: TTabSheet
      ExplicitWidth = 406
      ExplicitHeight = 323
      object lblLastName: TLabel
        Left = 24
        Top = 107
        Width = 50
        Height = 13
        Caption = 'Last Name'
        FocusControl = dbeLastName
      end
      object lblEmployeeID: TLabel
        Left = 24
        Top = 26
        Width = 11
        Height = 13
        Caption = 'ID'
        FocusControl = dbeEmployeeID
      end
      object lblFirstName: TLabel
        Left = 24
        Top = 67
        Width = 51
        Height = 13
        Caption = 'First Name'
        FocusControl = dbeFirstName
      end
      object dbeLastName: TDBEdit
        Left = 93
        Top = 104
        Width = 172
        Height = 21
        DataField = 'LASTNAME'
        DataSource = dscMain
        TabOrder = 2
      end
      object dbeEmployeeID: TDBEdit
        Left = 93
        Top = 23
        Width = 73
        Height = 21
        DataField = 'EMPLOYEEID'
        DataSource = dscMain
        TabOrder = 0
      end
      object dbeFirstName: TDBEdit
        Left = 93
        Top = 64
        Width = 172
        Height = 21
        DataField = 'FIRSTNAME'
        DataSource = dscMain
        TabOrder = 1
      end
    end
    object tabWork: TTabSheet
      Caption = 'Work'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter2: TSplitter
        Left = 0
        Top = 181
        Width = 406
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 161
        ExplicitWidth = 162
      end
      object pnlGrid: TPanel
        Left = 0
        Top = 0
        Width = 406
        Height = 181
        Align = alClient
        TabOrder = 0
        object pnlWorkNavigator: TPanel
          Left = 1
          Top = 139
          Width = 404
          Height = 41
          Align = alBottom
          TabOrder = 0
          object navWork: TDBNavigator
            Left = 11
            Top = 9
            Width = 240
            Height = 25
            DataSource = dscWork
            TabOrder = 0
          end
        end
        object grdWork: TDBGrid
          Left = 1
          Top = 1
          Width = 404
          Height = 138
          Align = alClient
          DataSource = dscWork
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 184
        Width = 406
        Height = 139
        Align = alBottom
        TabOrder = 1
        object lblProject: TLabel
          Left = 24
          Top = 32
          Width = 34
          Height = 13
          Caption = 'Project'
        end
        object lblStartDate: TLabel
          Left = 24
          Top = 64
          Width = 50
          Height = 13
          Caption = 'Start Date'
          FocusControl = dbeStartDate
        end
        object lblEndDate: TLabel
          Left = 200
          Top = 64
          Width = 44
          Height = 13
          Caption = 'End Date'
          FocusControl = dbeEndDate
        end
        object lblHours: TLabel
          Left = 24
          Top = 104
          Width = 28
          Height = 13
          Caption = 'Hours'
          FocusControl = dbeHours
        end
        object dbeStartDate: TDBEdit
          Left = 80
          Top = 64
          Width = 89
          Height = 21
          DataField = 'STARTDATE'
          DataSource = dscWork
          TabOrder = 1
        end
        object dbeEndDate: TDBEdit
          Left = 256
          Top = 64
          Width = 89
          Height = 21
          DataField = 'ENDDATE'
          DataSource = dscWork
          TabOrder = 2
        end
        object dbeHours: TDBEdit
          Left = 80
          Top = 101
          Width = 89
          Height = 21
          DataField = 'HOURS'
          DataSource = dscWork
          TabOrder = 3
        end
        object lupProject: TDBLookupComboBox
          Left = 80
          Top = 24
          Width = 185
          Height = 21
          DataField = 'PROJECTID'
          DataSource = dscWork
          KeyField = 'PROJECTID'
          ListField = 'NAME'
          ListSource = dscProjectLU
          TabOrder = 0
        end
      end
    end
  end
  inherited dscMain: TDataSource
    OnDataChange = dscMainDataChange
  end
  object dscWork: TDataSource
    Left = 56
    Top = 232
  end
  object dscProjectLU: TDataSource
    Left = 56
    Top = 304
  end
end
