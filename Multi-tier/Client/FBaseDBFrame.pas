unit FBaseDBFrame;

interface

uses
  System.Classes,
  System.Actions,
  Data.DB,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.DBCtrls,
  Vcl.ComCtrls,
  Vcl.ExtCtrls,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.StdCtrls,
  Vcl.Buttons,
  Vcl.ActnList,
  Vcl.ImgList,
  FBaseFrame,
  DBase;

type
  TfraBaseDB = class(TfraBase)
    pnlFilter: TPanel;
    pnlNavigator: TPanel;
    grdMain: TDBGrid;
    Splitter1: TSplitter;
    pgcRecordView: TPageControl;
    tabRecordView: TTabSheet;
    navMain: TDBNavigator;
    dscMain: TDataSource;
    btnSearchDB: TBitBtn;
    btnApplyUpdates: TSpeedButton;
    procedure btnApplyUpdatesClick(Sender: TObject);
    procedure dscMainStateChange(Sender: TObject);
  private
    { Private declarations }
  protected
    FdmoBase: TdmoBase;
    procedure EnableApplyUpdateButton;
  public
    { Public declarations }
    procedure InitialiseDataAccess; virtual; abstract;
    procedure SetFocus; virtual; abstract;
  end;

var
  fraBaseDB: TfraBaseDB;

implementation

{$R *.dfm}

procedure TfraBaseDB.btnApplyUpdatesClick(Sender: TObject);
begin
  inherited;
  FdmoBase.ApplyUpdates;
  EnableApplyUpdateButton;
end;

procedure TfraBaseDB.dscMainStateChange(Sender: TObject);
begin
  inherited;
  EnableApplyUpdateButton;
end;

procedure TfraBaseDB.EnableApplyUpdateButton;
begin
  if Assigned(FdmoBase) then
    btnApplyUpdates.Enabled := FdmoBase.HasPendingChanges;
end;

end.
