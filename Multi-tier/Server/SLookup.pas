unit SLookup;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.Provider,
  Datasnap.DBClient,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  SBase,
  UFDDataSetProvider,
  UFDDataSnapQuery;

type
  TsvmLookup = class(TsvmBase)
    qryProject: TFDDataSnapQuery;
    dspProject: TFDDataSetProvider;
    qryProjectPROJECTID: TIntegerField;
    qryProjectNAME: TStringField;
    qryProjectUPDNO: TIntegerField;
    procedure dspProjectBeforeUpdateRecord(Sender: TObject;
                                          SourceDS: TDataSet;
                                          DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind;
                                          var Applied: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  System.SysUtils,
  System.Variants;

{$R *.dfm}

procedure TsvmLookup.dspProjectBeforeUpdateRecord(Sender: TObject;
                                                 SourceDS: TDataSet;
                                                 DeltaDS: TCustomClientDataSet;
                                                 UpdateKind: TUpdateKind;
                                                 var Applied: Boolean);
var
  NewPK: Integer;
begin
  inherited;
  SetPKAndUpdNo(SourceDS, DeltaDS, UpdateKind, NewPK);
end;

end.
