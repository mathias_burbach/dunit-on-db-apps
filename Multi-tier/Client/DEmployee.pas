unit DEmployee;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.FMTBcd,
  Data.SqlExpr,
  Data.DB,
  Datasnap.DBClient,
  Datasnap.DSConnect,
  DBase;

type
  TdmoEmployee = class(TdmoBase)
    dpcEmployee: TDSProviderConnection;
    cdsEmployee: TClientDataSet;
    cdsEmployeeEMPLOYEEID: TIntegerField;
    cdsEmployeeFIRSTNAME: TStringField;
    cdsEmployeeLASTNAME: TStringField;
    cdsEmployeeUPDNO: TIntegerField;
    cdsEmployeeqryProjectWork: TDataSetField;
    cdsProjectWork: TClientDataSet;
    cdsProjectWorkPROJECTWORKID: TIntegerField;
    cdsProjectWorkEMPLOYEEID: TIntegerField;
    cdsProjectWorkPROJECTID: TIntegerField;
    cdsProjectWorkSTARTDATE: TDateField;
    cdsProjectWorkENDDATE: TDateField;
    cdsProjectWorkHOURS: TFloatField;
    cdsProjectWorkPROCESSEDSTMP: TSQLTimeStampField;
    cdsProjectWorkUPDNO: TIntegerField;
    dpcLookup: TDSProviderConnection;
    cdsProjectLU: TClientDataSet;
    cdsProjectWorkProject: TStringField;
    ssmExportWorkingHoursToCSV: TSqlServerMethod;
    procedure DataModuleCreate(Sender: TObject);
    procedure qryWorkNewRecord(DataSet: TDataSet);
    procedure cdsProjectWorkNewRecord(DataSet: TDataSet);
    procedure cdsEmployeeNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FLastName, FFirstName: string;
  public
    { Public declarations }
    procedure Open(const LastName, FirstName: string);
    function ExportWorkingHoursToCSV: string;
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  DMain;

{$R *.dfm}

{ TdmoEmployee }

procedure TdmoEmployee.cdsEmployeeNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsEmployeeEMPLOYEEID.AsInteger := GetNextTemporaryPK;
end;

procedure TdmoEmployee.cdsProjectWorkNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsProjectWorkPROJECTWORKID.AsInteger := GetNextTemporaryPK;
  cdsProjectWorkSTARTDATE.AsDateTime := Date;
end;

procedure TdmoEmployee.DataModuleCreate(Sender: TObject);
begin
  inherited;
  MainCDS := cdsEmployee;
  FLastName := '???';
  FFirstName := '???';
end;


function TdmoEmployee.ExportWorkingHoursToCSV: string;
begin
  ssmExportWorkingHoursToCSV.ExecuteMethod;
  try
    Result := ssmExportWorkingHoursToCSV.Params[0].AsString;
  finally
    ssmExportWorkingHoursToCSV.Close;
  end;
end;

procedure TdmoEmployee.Open(const LastName, FirstName: string);
begin
  if (FLastName <> LastName) Or (FFirstName <> FirstName) then
  begin
    cdsEmployee.DisableControls;
    try
      cdsEmployee.Close;
      try
        if (LastName = EmptyStr) and (FirstName = EmptyStr) then
        begin
          cdsEmployee.Params.ParamByName('LastName').AsString := LastName;
          cdsEmployee.Params.ParamByName('FirstName').AsString := FirstName;
        end
        else
        begin
          cdsEmployee.Params.ParamByName('LastName').AsString := LastName + '%';
          cdsEmployee.Params.ParamByName('FirstName').AsString := FirstName + '%';
        end;
      finally
        cdsEmployee.Open;
      end;
      FLastName := LastName;
      FFirstName := FirstName;
    finally
      cdsEmployee.EnableControls;
    end;
  end;
end;

procedure TdmoEmployee.qryWorkNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsProjectWorkSTARTDATE.AsDateTime := Date;
end;

end.
