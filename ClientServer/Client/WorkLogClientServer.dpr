program WorkLogClientServer;

uses
  Vcl.Forms,
  FMain in 'FMain.pas' {frmMain},
  FBaseFrame in 'FBaseFrame.pas' {fraBase: TFrame},
  FBaseDBFrame in 'FBaseDBFrame.pas' {fraBaseDB: TFrame},
  DMain in 'DMain.pas' {dmoMain: TDataModule},
  DBase in 'DBase.pas' {dmoBase: TDataModule},
  DEmployee in 'DEmployee.pas' {dmoEmployee: TDataModule},
  FEmployeeFrame in 'FEmployeeFrame.pas' {fraEmployee: TFrame},
  FLookupFrame in 'FLookupFrame.pas' {fraLookup: TFrame},
  DLookup in 'DLookup.pas' {dmoLookup: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TdmoMain, dmoMain);
  Application.Run;
end.
