program WorkLogServer;

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FServerMain in 'FServerMain.pas' {frmServerMain},
  SEmployee in 'SEmployee.pas' {svmEmployee: TDSServerModule},
  UServerContainer in 'UServerContainer.pas' {srvContainer: TDataModule},
  SBase in 'SBase.pas' {svmBase: TDSServerModule},
  SLookup in 'SLookup.pas' {svmLookup: TDSServerModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmServerMain, frmServerMain);
  Application.CreateForm(TsrvContainer, srvContainer);
  Application.Run;
end.

