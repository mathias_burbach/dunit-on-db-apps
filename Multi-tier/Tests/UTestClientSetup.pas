unit UTestClientSetup;

interface

uses
  Winapi.ShellApi,
  TestFrameWork,
  TestExtensions;

type
  TClientTestSetup = class(TTestSetup)
  strict private
    FAppServerStarted: Boolean;
    FShellInfo: TShellExecuteInfo;
    procedure StartAppServer;
    procedure StopAppServer;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  end;


implementation

{ TClientTestSetup }

uses
  System.SysUtils,
  Vcl.Forms,
  Winapi.Windows,
  DMain;

procedure TClientTestSetup.SetUp;
begin
  inherited;
  StartAppServer;
  dmoMain := TdmoMain.Create(nil);
end;

procedure TClientTestSetup.StartAppServer;
var
  sFileName: string;
begin
  sFileName := ExpandFileName(ExtractFilePath(ParamStr(0)) +'..\..\..\Server\Win32\Debug\WorkLogServer.exe');
  FillChar(FShellInfo, SizeOf(TShellExecuteInfo), 0);
  FShellInfo.cbSize := SizeOf(TShellExecuteInfo);
  FShellInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI or SEE_MASK_FLAG_DDEWAIT;
  FShellInfo.Wnd := Application.Handle;
  FShellInfo.lpVerb := PChar('open');
  FShellInfo.lpFile := PChar(sFileName);
  FShellInfo.lpParameters := nil;
  FShellInfo.lpDirectory := PChar(ExtractFilePath(sFileName));
  FShellInfo.nShow := SW_SHOWMINIMIZED;
  FAppServerStarted := ShellExecuteEx(@FShellInfo);
end;

procedure TClientTestSetup.StopAppServer;
begin
  if FAppServerStarted then
    TerminateProcess(FShellInfo.hProcess, 0);
end;

procedure TClientTestSetup.TearDown;
begin
  dmoMain.Free;
  StopAppServer;
  inherited;
end;

end.
