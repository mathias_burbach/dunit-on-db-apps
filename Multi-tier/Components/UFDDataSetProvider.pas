unit UFDDataSetProvider;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.Provider,
  Datasnap.DBClient;

type
  // DataSetProvider to use existing SQL in order to support TClientDataSet.RefreshRecord
  // will also support a version number on the record called UpdNo when deleting records
  TFDDataSetProvider = class(TDataSetProvider)
  private
    { Private declarations }
    FUseSQLForRefreshRecord: Boolean;
    procedure SetUseSQLForRefreshRecord(const Value: Boolean);
    procedure GetPKColumnNameAndValue(const Source, Delta: TDataSet;
                                      var PKColumnName: string;
                                      var PKColumnValue: Integer);
    procedure ReplaceWhereClause(const RefreshSQL: TStringList;
                                 const PKColumnName: string;
                                 const PKColumnValue: Integer);
    function GenSelectSQL(const Source, Delta: TDataSet): string;
    function GetDataRecordFromSource(const Source, Delta: TDataSet): TDataSet;
    function HasUpdNoColumn(const SourceDS: TDataSet): Boolean;
    function FindFromTableName(const OriginalSQL: TStringList): string;
    procedure DoDeleteWithUpdNo(const SourceDS, DeltaDS: TDataSet);
    function FindFromTableNameAlias(const OriginalSQL: TStringList): string;
  protected
    { Protected declarations }
    procedure UpdateRecord(Source, Delta: TDataSet;
                           BlobsOnly, KeyOnly: Boolean); override;
    procedure DoBeforeUpdateRecord(SourceDS: TDataSet;
                                   DeltaDS: TCustomClientDataSet;
                                   UpdateKind: TUpdateKind;
                                   var Applied: Boolean); override;

  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property UseSQLForRefreshRecord: Boolean read FUseSQLForRefreshRecord write SetUseSQLForRefreshRecord;
  end;

  procedure Register;

implementation

uses
  System.SysUtils,
  System.Variants,
  System.TypInfo,
  System.StrUtils,
  Data.DBConsts;

procedure Register;
begin
  RegisterComponents('FD-DataSnap', [TFDDataSetProvider]);
end;

{ TFDDataSetProvider }

constructor TFDDataSetProvider.Create(AOwner: TComponent);
begin
  inherited;
  FUseSQLForRefreshRecord := True;
  Options := [poPropogateChanges];
  UpdateMode := upWhereChanged;
end;

procedure TFDDataSetProvider.DoBeforeUpdateRecord(SourceDS: TDataSet;
  DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  inherited;
  if (not Applied) and
     (UpdateKind = ukDelete) and
     HasUpdNoColumn(DeltaDS) then
  begin
    DoDeleteWithUpdNo(SourceDS, DeltaDS);
    Applied := True;
  end;
end;

procedure TFDDataSetProvider.DoDeleteWithUpdNo(const SourceDS, DeltaDS: TDataSet);
var
  sTableName, sPKColumnName, sSQL: string;
  OriginalSQL: TStringList;
  iPKColumnValue, iOldUpdNo, iRowsAffected: Integer;
  Params: TParams;
begin
  DoGetTableName(SourceDS, sTableName);
  if sTableName = EmptyStr then
  begin
    OriginalSQL := GetObjectProp(SourceDS, 'SQL') as TStringList;
    sTableName := FindFromTableName(OriginalSQL);
  end;
  GetPKColumnNameAndValue(DeltaDS, DeltaDS, sPKColumnName, iPKColumnValue);
  iOldUpdNo := DeltaDS.FieldByName('UpdNo').AsInteger;
  sSQL := Format('Delete From %s Where %s = %d And UpdNo = %d',
                 [sTableName, sPKColumnName, iPKColumnValue, iOldUpdNo]);
  Params := TParams.Create(Self);
  try
    iRowsAffected := (SourceDS as IProviderSupportNG).PSExecuteStatement(sSQL, Params);
    if iRowsAffected <> 1 then
      raise Exception.CreateFmt('Could not delete record in table %s', [sTableName]);
  finally
    Params.Free;
  end;
end;

function TFDDataSetProvider.FindFromTableName(const OriginalSQL: TStringList): string;
var
  sLine: string;
  i, iPos: Integer;
begin
  Result := EmptyStr;
  for i := 0 to OriginalSQL.Count-1 do
  begin
    sLine := Trim(OriginalSQL[i]);
    if UpperCase(Copy(sLine, 1, 5)) = 'FROM ' then
    begin
      Result := Copy(sLine, 6, 255);
      iPos := Pos(' ', Result);
      if iPos > 0 then
        Result := Copy(Result, 1, iPos-1);
      Break;
    end;
  end;
end;

function TFDDataSetProvider.FindFromTableNameAlias(const OriginalSQL: TStringList): string;
var
  sLine: string;
  i, iPos: Integer;
begin
  Result := EmptyStr;
  for i := 0 to OriginalSQL.Count-1 do
  begin
    sLine := OriginalSQL[i];
    if UpperCase(Copy(sLine, 1, 5)) = 'FROM ' then // ignore indented FROM's.
    begin
      Result := Copy(sLine, 6, 255);
      iPos := Pos(' ', Result);
      if iPos > 0 then
        Result := Copy(Result, iPos + 1, Length(Result));
      Break;
    end;
  end;
end;

function TFDDataSetProvider.GenSelectSQL(const Source, Delta: TDataSet): string;
var
  PKColumnName, FromTableNameAlias: string;
  PKColumnValue: Integer;
  OriginalSQL, RefreshSQL: TStringList;
begin
  GetPKColumnNameAndValue(Source, Delta, PKColumnName, PKColumnValue);
  OriginalSQL := GetObjectProp(Source, 'SQL') as TStringList;
  if Pos('.', PKColumnName) = 0 then // if the PKColumnName does not already contain an alias.
  begin
    FromTableNameAlias := FindFromTableNameAlias(OriginalSQL);
    if FromTableNameAlias <> EmptyStr then
      PKColumnName := FromTableNameAlias + '.' + PKColumnName;
  end;
  RefreshSQL := TStringList.Create;
  try
    RefreshSQL.Assign(OriginalSQL);
    ReplaceWhereClause(RefreshSQL, PKColumnName, PKColumnValue);
    Result := RefreshSQL.Text;
  finally
    RefreshSQL.Free;
  end;
end;

function TFDDataSetProvider.GetDataRecordFromSource(const Source, Delta: TDataSet): TDataSet;
var
  SQL: String;
  Params: TParams;
begin
  Result := nil;
  SQL := GenSelectSQL(Source, Delta);
  Params := TParams.Create(nil);
  try
    IProviderSupportNG(Source).PSExecuteStatement(SQL, Params, Result);
  finally
    Params.Free;
  end;
  if Result.EOF then
    DatabaseError(SRecordChanged);
end;

procedure TFDDataSetProvider.GetPKColumnNameAndValue(const Source, Delta: TDataSet;
                                                       var PKColumnName: string;
                                                       var PKColumnValue: Integer);
var
  i: Integer;
begin
  for i := 0 to Source.Fields.Count-1 do
    if pfInKey In Source.Fields[i].ProviderFlags then
    begin
      if Source.Fields[i].Origin <> EmptyStr then
        PKColumnName := Source.Fields[i].Origin
      else
        PKColumnName := Source.Fields[i].FieldName;
      PKColumnValue := Delta.Fields[i].AsInteger;
      Break;
    end;
end;

function TFDDataSetProvider.HasUpdNoColumn(const SourceDS: TDataSet): Boolean;
begin
  Result := (SourceDS.FindField('UpdNo') <> nil);
end;

procedure TFDDataSetProvider.ReplaceWhereClause(const RefreshSQL: TStringList;
                                                  const PKColumnName: string;
                                                  const PKColumnValue: Integer);
var
  i, WhereStartLine, OrderByStartLine, DeleteStart, DeleteEnd: Integer;
  WhereClause: string;
begin
  WhereClause := Format('Where %s = %d', [PKColumnName, PKColumnValue]);
  WhereStartLine := -1;
  OrderByStartLine := -1;
  for i := 0 to RefreshSQL.Count-1 do
    if UpperCase(Copy(RefreshSQL[i], 1, 6)) = 'WHERE ' then
      WhereStartLine := i
    else if UpperCase(Copy(RefreshSQL[i], 1, 9)) = 'ORDER BY ' then
      OrderByStartLine := i;
  if WhereStartLine = -1 then
  begin
    if OrderByStartLine = -1 then
      RefreshSQL.Add(WhereClause)
    else
      RefreshSQL.Insert(OrderByStartLine, WhereClause);
  end
  else
  begin
    RefreshSQL[WhereStartLine] := WhereClause;
    DeleteStart := WhereStartLine + 1;
    if OrderByStartLine = -1 then
      DeleteEnd := RefreshSQL.Count-1
    else
      DeleteEnd := OrderByStartLine-1;
    for i := DeleteEnd downto DeleteStart do
      RefreshSQL.Delete(i);
  end;
end;

procedure TFDDataSetProvider.SetUseSQLForRefreshRecord(const Value: Boolean);
begin
  FUseSQLForRefreshRecord := Value;
end;

procedure TFDDataSetProvider.UpdateRecord(Source, Delta: TDataSet;
                                            BlobsOnly,  KeyOnly: Boolean);
var
  Field: TField;
  i: Integer;
  DS: TDataSet;
begin
  if FUseSQLForRefreshRecord then
  begin
    DS := GetDataRecordFromSource(Source, Delta);
    try
      Delta.Edit;
      for i := 0 to Source.FieldCount - 1 do
      begin
        Field := DS.FindField(Source.Fields[i].FieldName);
        if (Field <> nil) and
           (not (Field.Lookup or Field.Calculated)) and
           (not BlobsOnly or (Field.IsBlob and VarIsNull(Delta.Fields[i].NewValue))) and
           (not (pfInKey in Source.Fields[i].ProviderFlags)) then
          Delta.Fields[i].Assign(Field);
      end;
      Delta.Post;
    finally
      DS.Free;
    end;
  end
  else
    inherited;
end;

end.

