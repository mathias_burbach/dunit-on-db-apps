/*
Created		13/08/2014
Modified		13/08/2014
Project		
Model		
Company		
Author		
Version		
Database		Firebird 
*/


Create Domain DPK As Integer;
Create Domain DName As Varchar(20);
Create Domain DDateOnly As Date;
Create Domain DVersionNo As Integer;
Create Domain DHours As Double precision;
Create Domain DTimestamp As Timestamp;


Create Table Employee  (
	EmployeeID Integer NOT NULL,
	FirstName Varchar(20) NOT NULL,
	LastName Varchar(20) NOT NULL,
	UpdNo Integer NOT NULL,
 Primary Key (EmployeeID)
);

Create Table Project  (
	ProjectID Integer NOT NULL,
	Name Varchar(20) NOT NULL,
	UpdNo Integer NOT NULL,
 Primary Key (ProjectID)
);

Create Table ProjectWork  (
	ProjectWorkID Integer NOT NULL,
	EmployeeID Integer NOT NULL,
	ProjectID Integer NOT NULL,
	StartDate Date NOT NULL,
	EndDate Date,
	Hours Double precision,
	ProcessedStmp Timestamp,
	UpdNo Integer NOT NULL,
 Primary Key (ProjectWorkID)
);


Alter Table Project add Constraint AK_Project UNIQUE (Name);


Create Index IX_Employee_LastName  ON Employee (LastName);
Create Index IX_Employee_FirstName  ON Employee (FirstName);
Create Index IX_ProjectWork_StartDate  ON ProjectWork (StartDate);


Alter Table ProjectWork Add Constraint FK_Employee_ProjectWork Foreign Key (EmployeeID) References Employee (EmployeeID) On Update No Action On Delete No Action ;
Alter Table ProjectWork Add Constraint FK_Project_ProjectWork Foreign Key (ProjectID) References Project (ProjectID) On Update No Action On Delete No Action ;


Create Generator GenEmployeeID;

Create Generator GenProjectID;

Create Generator GenProjectWorkID;


