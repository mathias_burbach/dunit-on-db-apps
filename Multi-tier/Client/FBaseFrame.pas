unit FBaseFrame;

interface

uses
  Vcl.Forms;

type
  TfraBase = class(TFrame)
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure DoApplyUpdateError(const ErrorMessage: string);
  public
    { Public declarations }
  end;

implementation

uses
  Vcl.Dialogs;

{$R *.dfm}

{ TfraBase }

procedure TfraBase.DoApplyUpdateError(const ErrorMessage: string);
begin
  ShowMessage(ErrorMessage);
end;

end.
