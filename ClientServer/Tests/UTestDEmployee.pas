unit UTestDEmployee;

interface

uses
  UTestBase,
  DEmployee;

type
  TTestdmoEmployee = class(TBaseTestCase)
  private
    dmoEmployee: TdmoEmployee;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestOpen;
    procedure TestAdd;
    procedure TestEdit;
    procedure TestDelete;
    procedure TestOpenWork;
    procedure TestAddWork;
    procedure TestEditWork;
    procedure TestDeleteWork;
    procedure TestExportWorkingHoursToCSV;
  end;

implementation

uses
  System.SysUtils,
  System.Classes,
  DTestHelper;

{ TTestdmoEmployee }

procedure TTestdmoEmployee.SetUp;
begin
  inherited;
  dmoEmployee := TdmoEmployee.Create(nil);
end;

procedure TTestdmoEmployee.TearDown;
begin
  dmoEmployee.Free;
  inherited;
end;

procedure TTestdmoEmployee.TestAdd;
const
  cLastName = 'Burbach';
  cFirstName = 'Mathias';
  cNextEmployeeID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, cFirstName);
  CheckEquals(0, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  dmoEmployee.qryEmployee.Append;
  dmoEmployee.qryEmployeeLASTNAME.AsString := cLastName;
  dmoEmployee.qryEmployeeFIRSTNAME.AsString := cFirstName;
  dmoEmployee.qryEmployee.Post;
  CheckEquals(0, dmoEmployee.qryEmployeeUPDNO.AsInteger, 'UpdNo');
  CheckEquals(cNextEmployeeID, dmoEmployee.qryEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cLastName, DBValue, 'LastName');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'FirstName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cFirstName, DBValue, 'FirstName');
end;

procedure TTestdmoEmployee.TestAddWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectID = 3;
  cNextProjectWorkID = 4;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.qryEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.WorkActive := True;
  dmoEmployee.qryWork.Last;
  CheckEquals(2, dmoEmployee.qryWork.RecordCount, 'qryWork.RecordCount');
  dmoEmployee.qryWork.Append;
  CheckEquals(Date, dmoEmployee.qryWorkSTARTDATE.AsDateTime, 'StartDate');
  dmoEmployee.qryWorkPROJECTID.AsInteger := cProjectID;
  dmoEmployee.qryWorkHOURS.AsFloat := cHours;
  dmoEmployee.qryWork.Post;
  CheckEquals(cNextProjectWorkID, dmoEmployee.qryWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  CheckEquals(0, dmoEmployee.qryWorkUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'StartDate', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  CheckEquals(DateToStr(Date), DBValue, 'StartDate');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  CheckEquals(FloatToStr(cHours), DBValue, 'Hours');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProjectID', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  CheckEquals(IntToStr(cProjectID), DBValue, 'ProjectID');
end;

procedure TTestdmoEmployee.TestDelete;
const
  cLastName = 'Smith';
  cEmployeeID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.qryEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.qryEmployee.Delete;
  DBValue := dmoTestHelper.GetDBValue('Employee', 'Count(1)', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals('0', DBValue, 'LastName');
end;

procedure TTestdmoEmployee.TestDeleteWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.qryEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.WorkActive := True;
  dmoEmployee.qryWork.Last;
  CheckEquals(2, dmoEmployee.qryWork.RecordCount, 'qryWork.RecordCount');
  CheckEquals(cProjectWorkID, dmoEmployee.qryWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  dmoEmployee.qryWork.Delete;
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Count(1)', Format('ProjectWorkID=%d', [cProjectWorkID]));
  CheckEquals('0', DBValue, 'Count');
end;

procedure TTestdmoEmployee.TestEdit;
const
  cLastName = 'B';
  cChangedName = 'NewLastName';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.qryEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.qryEmployee.Edit;
  dmoEmployee.qryEmployeeLASTNAME.AsString := cChangedName;
  dmoEmployee.qryEmployee.Post;
  CheckEquals(1, dmoEmployee.qryEmployeeUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals(cChangedName, DBValue, 'LastName');
end;

procedure TTestdmoEmployee.TestEditWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.qryEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.WorkActive := True;
  dmoEmployee.qryWork.Last;
  CheckEquals(2, dmoEmployee.qryWork.RecordCount, 'qryWork.RecordCount');
  CheckEquals(cProjectWorkID, dmoEmployee.qryWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  dmoEmployee.qryWork.Edit;
  dmoEmployee.qryWorkENDDATE.AsDateTime := Date;
  dmoEmployee.qryWorkHOURS.AsFloat := cHours;
  dmoEmployee.qryWork.Post;
  CheckEquals(1, dmoEmployee.qryWorkUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'EndDate', Format('ProjectWorkID=%d', [cProjectWorkID]));
  CheckEquals(DateToStr(Date), DBValue, 'EndDate');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cProjectWorkID]));
  CheckEquals(FloatToStr(cHours), DBValue, 'Hours');
end;

procedure TTestdmoEmployee.TestExportWorkingHoursToCSV;
const
  cFileName = 'Export.csv';
  caCSV: array[0..1] of string =
    ('EMPLOYEEID,FIRSTNAME,LASTNAME,PROJECT,STARTDATE,ENDDATE,HOURS',
     '1,"Joe","Bloggs","Project A",14/07/2014,18/07/2014,40');
var
  DBValue: string;
  stlCSV: TStringList;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.ExportWorkingHoursToCSV(cFileName);
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProcessedStmp', 'ProjectWorkID=1');
  CheckTrue(DBValue <> EmptyStr, 'ProcessedStmp Is Not Null');
  stlCSV := TStringList.Create;
  try
    stlCSV.LoadFromFile(cFileName);
    CheckEquals(2, stlCSV.Count, 'CSV.Count');
    CheckEquals(caCSV[0], stlCSV[0], 'stlCSV[0]');
    CheckEquals(caCSV[1], stlCSV[1], 'stlCSV[1]');
  finally
    stlCSV.Free;
  end;
end;

procedure TTestdmoEmployee.TestOpen;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  dmoEmployee.Open(cAnyValue, EmptyStr);
  CheckEquals(3, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
end;

procedure TTestdmoEmployee.TestOpenWork;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.qryEmployee.RecordCount, 'qryEmployee.RecordCount');
  dmoEmployee.WorkActive := True;
  CheckEquals(2, dmoEmployee.qryWork.RecordCount, 'qryWork.RecordCount');
end;

end.
