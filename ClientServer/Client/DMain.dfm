object dmoMain: TdmoMain
  OnCreate = DataModuleCreate
  Height = 150
  Width = 243
  object conFB: TFDConnection
    Params.Strings = (
      'Database=WorkLog'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    UpdateOptions.AssignedValues = [uvRefreshDelete]
    UpdateOptions.RefreshDelete = False
    ConnectedStoredUsage = [auDesignTime]
    Connected = True
    LoginPrompt = False
    Left = 56
    Top = 40
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 160
    Top = 40
  end
end
