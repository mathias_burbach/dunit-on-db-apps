inherited svmEmployee: TsvmEmployee
  Height = 306
  Width = 412
  object qryEmployee: TFDDataSnapQuery
    Connection = conFB
    FetchOptions.AssignedValues = [evMode, evCursorKind]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvRefreshMode, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.UpdateChangedFields = False
    UpdateOptions.UpdateMode = upWhereChanged
    UpdateOptions.RefreshMode = rmManual
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'Select *'
      'From Employee'
      'Where LastName Like :LastName'
      '  And FirstName Like :FirstName'
      'Order By LastName, FirstName')
    Left = 160
    Top = 40
    ParamData = <
      item
        Name = 'LASTNAME'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end
      item
        Name = 'FIRSTNAME'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end>
    object qryEmployeeEMPLOYEEID: TIntegerField
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qryEmployeeFIRSTNAME: TStringField
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
    end
    object qryEmployeeLASTNAME: TStringField
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
    end
    object qryEmployeeUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
    end
  end
  object dspEmployee: TFDDataSetProvider
    DataSet = qryEmployee
    Options = [poPropogateChanges]
    UpdateMode = upWhereChanged
    BeforeUpdateRecord = dspEmployeeBeforeUpdateRecord
    BeforeApplyUpdates = dspEmployeeBeforeApplyUpdates
    AfterApplyUpdates = dspEmployeeAfterApplyUpdates
    AfterGetRecords = dspEmployeeAfterGetRecords
    UseSQLForRefreshRecord = True
    Left = 264
    Top = 40
  end
  object dscEmployee: TDataSource
    DataSet = qryEmployee
    Left = 160
    Top = 112
  end
  object qryProjectWork: TFDDataSnapQuery
    MasterSource = dscEmployee
    MasterFields = 'EMPLOYEEID'
    Connection = conFB
    FetchOptions.AssignedValues = [evMode, evCursorKind]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvRefreshMode, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.UpdateChangedFields = False
    UpdateOptions.UpdateMode = upWhereChanged
    UpdateOptions.RefreshMode = rmManual
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'Select *'
      'From ProjectWork'
      'Where EmployeeID = :EmployeeID'
      'Order By StartDate')
    Left = 264
    Top = 112
    ParamData = <
      item
        Name = 'EMPLOYEEID'
        DataType = ftInteger
        ParamType = ptInput
        Size = 4
        Value = Null
      end>
    object qryProjectWorkPROJECTWORKID: TIntegerField
      FieldName = 'PROJECTWORKID'
      Origin = 'PROJECTWORKID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qryProjectWorkEMPLOYEEID: TIntegerField
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      Required = True
    end
    object qryProjectWorkPROJECTID: TIntegerField
      FieldName = 'PROJECTID'
      Origin = 'PROJECTID'
      Required = True
    end
    object qryProjectWorkSTARTDATE: TDateField
      FieldName = 'STARTDATE'
      Origin = 'STARTDATE'
      Required = True
    end
    object qryProjectWorkENDDATE: TDateField
      FieldName = 'ENDDATE'
      Origin = 'ENDDATE'
    end
    object qryProjectWorkHOURS: TFloatField
      FieldName = 'HOURS'
      Origin = 'HOURS'
    end
    object qryProjectWorkPROCESSEDSTMP: TSQLTimeStampField
      FieldName = 'PROCESSEDSTMP'
      Origin = 'PROCESSEDSTMP'
    end
    object qryProjectWorkUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
    end
  end
  object qryExport: TFDQuery
    Connection = conFB
    FetchOptions.AssignedValues = [evMode, evUnidirectional]
    FetchOptions.Mode = fmAll
    FetchOptions.Unidirectional = True
    SQL.Strings = (
      'Select e.EmployeeID, e.FirstName, e.LastName, p.Name Project,'
      '  w.StartDate, w.EndDate, w.Hours, w.ProjectWorkID'
      'From ProjectWork w'
      '  Join Employee e'
      '    On w.EmployeeID = e.EmployeeID'
      '  Join Project p'
      '    On w.ProjectID = p.ProjectID'
      'Where w.EndDate Is Not Null'
      '  And w.ProcessedStmp Is Null'
      'Order By w.StartDate')
    Left = 48
    Top = 200
    object qryExportEMPLOYEEID: TIntegerField
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      Required = True
    end
    object qryExportFIRSTNAME: TStringField
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
    end
    object qryExportLASTNAME: TStringField
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
    end
    object qryExportPROJECT: TStringField
      FieldName = 'PROJECT'
      Origin = 'PROJECT'
      Required = True
    end
    object qryExportSTARTDATE: TDateField
      FieldName = 'STARTDATE'
      Origin = 'STARTDATE'
      Required = True
    end
    object qryExportENDDATE: TDateField
      FieldName = 'ENDDATE'
      Origin = 'ENDDATE'
    end
    object qryExportHOURS: TFloatField
      FieldName = 'HOURS'
      Origin = 'HOURS'
    end
    object qryExportPROJECTWORKID: TIntegerField
      FieldName = 'PROJECTWORKID'
      Origin = 'PROJECTWORKID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object cmdMarkProcessed: TFDCommand
    Connection = conFB
    Left = 136
    Top = 200
  end
end
