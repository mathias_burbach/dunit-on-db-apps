unit UTestBase;

interface

type
  TBaseTestCase = class(TObject)
  private
    FRunCreateFixtureAtTearDown: Boolean;
    procedure SetRunCreateFixtureAtTearDown(const Value: Boolean);
  public
    procedure SetUp;
    procedure TearDown;
    property RunCreateFixtureAtTearDown: Boolean read FRunCreateFixtureAtTearDown write SetRunCreateFixtureAtTearDown;
  end;

implementation

uses
  DTestHelper;

{ TBaseTestCase }

procedure TBaseTestCase.SetRunCreateFixtureAtTearDown(const Value: Boolean);
begin
  FRunCreateFixtureAtTearDown := True;
end;

procedure TBaseTestCase.SetUp;
begin
  FRunCreateFixtureAtTearDown := False;
end;

procedure TBaseTestCase.TearDown;
begin
  if FRunCreateFixtureAtTearDown then
  begin
    dmoTestHelper.CreateFixture;
    FRunCreateFixtureAtTearDown := False;
  end;
end;

end.
