inherited fraLookup: TfraLookup
  inherited Splitter1: TSplitter
    Left = 229
    ExplicitLeft = 229
  end
  inherited pnlFilter: TPanel
    object lblFilterByName: TLabel [0]
      Left = 432
      Top = 17
      Width = 32
      Height = 15
      Anchors = [akTop, akRight]
      Caption = 'Name'
      FocusControl = edtFilterByName
    end
    inherited btnSearchDB: TBitBtn
      Left = 572
      TabOrder = 1
      OnClick = btnSearchDBClick
      ExplicitLeft = 572
    end
    object edtFilterByName: TEdit
      Left = 465
      Top = 13
      Width = 97
      Height = 23
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
  end
  inherited pnlNavigator: TPanel
    inherited navMain: TDBNavigator
      Hints.Strings = ()
    end
  end
  inherited grdMain: TDBGrid
    Width = 229
  end
  inherited pgcRecordView: TPageControl
    Left = 232
    Width = 456
    ExplicitLeft = 232
    ExplicitWidth = 456
    inherited tabRecordView: TTabSheet
      ExplicitWidth = 448
      object lblID: TLabel
        Left = 24
        Top = 26
        Width = 11
        Height = 15
        Caption = 'ID'
        FocusControl = dbeID
      end
      object lblName: TLabel
        Left = 24
        Top = 67
        Width = 32
        Height = 15
        Caption = 'Name'
        FocusControl = dbeName
      end
      object dbeID: TDBEdit
        Left = 93
        Top = 23
        Width = 73
        Height = 23
        DataSource = dscMain
        TabOrder = 0
      end
      object dbeName: TDBEdit
        Left = 93
        Top = 64
        Width = 172
        Height = 23
        DataField = 'Name'
        DataSource = dscMain
        TabOrder = 1
      end
    end
  end
end
