unit UTestDLookup;

interface

uses
  UTestBase,
  DLookup;

type
  TTestdmoLookup = class(TBaseTestCase)
  private
    dmoLookup: TdmoLookup;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestOpen;
    procedure TestAdd;
    procedure TestEdit;
    procedure TestDelete;
  end;

implementation

uses
  System.SysUtils,
  DTestHelper;

{ TTestdmoLookup }

procedure TTestdmoLookup.SetUp;
begin
  inherited;
  dmoLookup := TdmoLookup.Create(nil, 'Project')
end;

procedure TTestdmoLookup.TearDown;
begin
  dmoLookup.Free;
  inherited;
end;

procedure TTestdmoLookup.TestAdd;
const
  cProject = 'Project D';
  cNextProjectID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoLookup.Open(cProject);
  CheckEquals(0, dmoLookup.qryLookup.RecordCount, 'qryLookup.RecordCount');
  dmoLookup.qryLookup.Append;
  dmoLookup.qryLookup.Fields[1].AsString := cProject;
  dmoLookup.qryLookup.Post;
  CheckEquals(cNextProjectID, dmoLookup.qryLookup.Fields[0].AsInteger, 'ID');
  DBValue := dmoTestHelper.GetDBValue('Project', 'Name', Format('ProjectID=%d', [cNextProjectID]));
  CheckEquals(cProject, DBValue, 'Name');
end;

procedure TTestdmoLookup.TestDelete;
const
  cProject = 'Project C';
  cProjectID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoLookup.Open(cProject);
  CheckEquals(1, dmoLookup.qryLookup.RecordCount, 'qryLookup.RecordCount');
  CheckEquals(cProjectID, dmoLookup.qryLookup.Fields[0].AsInteger, 'ID');
  dmoLookup.qryLookup.Delete;
  DBValue := dmoTestHelper.GetDBValue('Project', 'Count(1)', Format('ProjectID=%d', [cProjectID]));
  CheckEquals('0', DBValue, 'Count');
end;

procedure TTestdmoLookup.TestEdit;
const
  cProject = 'Project A';
  cChangedName = 'Project A1';
  cProjectID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoLookup.Open(cProject);
  CheckEquals(1, dmoLookup.qryLookup.RecordCount, 'qryLookup.RecordCount');
  CheckEquals(cProjectID, dmoLookup.qryLookup.Fields[0].AsInteger, 'ID');
  dmoLookup.qryLookup.Edit;
  dmoLookup.qryLookup.Fields[1].AsString := cChangedName;
  dmoLookup.qryLookup.Post;
  CheckEquals(1, dmoLookup.qryLookup.Fields[2].AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('Project', 'Name', Format('ProjectID=%d', [cProjectID]));
  CheckEquals(cChangedName, DBValue, 'Name');
end;

procedure TTestdmoLookup.TestOpen;
const
  cProject = 'Project';
  cNoProject = 'No Project';
begin
  dmoLookup.Open(cProject);
  CheckEquals(3, dmoLookup.qryLookup.RecordCount, 'qryLookup.RecordCount');
  dmoLookup.Open(cNoProject);
  CheckEquals(0, dmoLookup.qryLookup.RecordCount, 'qryLookup.RecordCount');
end;

end.
