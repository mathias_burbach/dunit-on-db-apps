unit FBaseDBFrame;

interface

uses
  System.Classes,
  Data.DB,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.DBCtrls,
  Vcl.ComCtrls,
  Vcl.ExtCtrls,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.StdCtrls,
  Vcl.Buttons,
  FBaseFrame;

type
  TfraBaseDB = class(TfraBase)
    pnlFilter: TPanel;
    pnlNavigator: TPanel;
    grdMain: TDBGrid;
    Splitter1: TSplitter;
    pgcRecordView: TPageControl;
    tabRecordView: TTabSheet;
    navMain: TDBNavigator;
    dscMain: TDataSource;
    btnSearchDB: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InitialiseDataAccess; virtual; abstract;
    procedure SetFocus; virtual; abstract;
  end;

var
  fraBaseDB: TfraBaseDB;

implementation

{$R *.dfm}

end.
