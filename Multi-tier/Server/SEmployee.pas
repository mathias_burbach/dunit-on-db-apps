unit SEmployee;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.DSServer,
  DataSnap.DSProviderDataModuleAdapter,
  Datasnap.Provider,
  Datasnap.DBClient,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Comp.Client,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  SBase,
  UFDDataSnapQuery,
  UFDDataSetProvider;

type
  {$MethodInfo ON}
  TsvmEmployee = class(TsvmBase)
    {$MethodInfo OFF}
    qryEmployee: TFDDataSnapQuery;
    qryEmployeeEMPLOYEEID: TIntegerField;
    qryEmployeeFIRSTNAME: TStringField;
    qryEmployeeLASTNAME: TStringField;
    qryEmployeeUPDNO: TIntegerField;
    dspEmployee: TFDDataSetProvider;
    dscEmployee: TDataSource;
    qryProjectWork: TFDDataSnapQuery;
    qryProjectWorkPROJECTWORKID: TIntegerField;
    qryProjectWorkEMPLOYEEID: TIntegerField;
    qryProjectWorkPROJECTID: TIntegerField;
    qryProjectWorkSTARTDATE: TDateField;
    qryProjectWorkENDDATE: TDateField;
    qryProjectWorkHOURS: TFloatField;
    qryProjectWorkPROCESSEDSTMP: TSQLTimeStampField;
    qryProjectWorkUPDNO: TIntegerField;
    qryExport: TFDQuery;
    qryExportEMPLOYEEID: TIntegerField;
    qryExportFIRSTNAME: TStringField;
    qryExportLASTNAME: TStringField;
    qryExportPROJECT: TStringField;
    qryExportSTARTDATE: TDateField;
    qryExportENDDATE: TDateField;
    qryExportHOURS: TFloatField;
    qryExportPROJECTWORKID: TIntegerField;
    cmdMarkProcessed: TFDCommand;
    procedure dspEmployeeBeforeUpdateRecord(Sender: TObject;
                                            SourceDS: TDataSet;
                                            DeltaDS: TCustomClientDataSet;
                                            UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure dspEmployeeAfterGetRecords(Sender: TObject;
                                        var OwnerData: OleVariant);
    procedure dspEmployeeAfterApplyUpdates(Sender: TObject;
                                           var OwnerData: OleVariant);
    procedure dspEmployeeBeforeApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
  private
    FEmployeeID: Integer;
    { Private declarations }
    procedure MarkWorkAsExported(const ProjectWorkIDs: string);
  public
    { Public declarations }
    {$MethodInfo ON}
    function ExportWorkingHoursToCSV: string;
  end;
  {$MethodInfo OFF}

implementation

{$R *.dfm}

uses
  System.SysUtils,
  System.StrUtils;

procedure TsvmEmployee.dspEmployeeAfterApplyUpdates(Sender: TObject;
                                                    var OwnerData: OleVariant);
begin
  inherited;
  {$IFDEF UnitTests}
  conFB.Close;
  {$ENDIF}
end;

procedure TsvmEmployee.dspEmployeeAfterGetRecords(Sender: TObject;
                                                  var OwnerData: OleVariant);
begin
  inherited;
  {$IFDEF UnitTests}
  conFB.Close;
  {$ENDIF}
end;

procedure TsvmEmployee.dspEmployeeBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  inherited;
  FEmployeeID := -1;
end;

procedure TsvmEmployee.dspEmployeeBeforeUpdateRecord(Sender: TObject;
                                                     SourceDS: TDataSet;
                                                     DeltaDS: TCustomClientDataSet;
                                                     UpdateKind: TUpdateKind;
                                                     var Applied: Boolean);
var
  NewPK: Integer;
begin
  inherited;
  SetPKAndUpdNo(SourceDS, DeltaDS, UpdateKind, NewPK);
  if UpdateKind = ukInsert then
  begin
    if SourceDS = qryEmployee then
      FEmployeeID := NewPK
    else if (SourceDS = qryProjectWork) and (FEmployeeID > 0) then
      DeltaDS.FieldByName('EmployeeID').NewValue := FEmployeeID;
  end;
end;

function TsvmEmployee.ExportWorkingHoursToCSV: string;
  function BuildCSVHeader: string;
  var
    i: Integer;
  begin
    Result := EmptyStr;
    for i := 0 to qryExport.FieldCount-2 do // do not export ProjectWorkID
      Result := Result + Format('%s,', [qryExport.Fields[i].FieldName]);
    Result := Copy(Result, 1, Length(Result)-1);
  end;
  function BuildCSVRow: string;
  var
    i: Integer;
  begin
    Result := EmptyStr;
    for i := 0 to qryExport.FieldCount-2 do // do not export ProjectWorkID
      if qryExport.Fields[i].DataType = ftString then
        Result := Result + Format('"%s",', [qryExport.Fields[i].AsString])
      else
        Result := Result + Format('%s,', [qryExport.Fields[i].AsString]);
    Result := Copy(Result, 1, Length(Result)-1);
  end;
var
  stlCSV: TStringList;
  sProjectWorkIDs: string;
begin
  Result := EmptyStr;
  qryExport.Open;
  try
    if not qryExport.Eof then
    begin
      stlCSV := TStringList.Create;
      try
        sProjectWorkIDs := EmptyStr;
        stlCSV.Add(BuildCSVHeader);
        while not qryExport.Eof do
        begin
          sProjectWorkIDs := sProjectWorkIDs +
                             Format('%d,', [qryExportPROJECTWORKID.AsInteger]);
          stlCSV.Add(BuildCSVRow);
          qryExport.Next;
        end;
        Result := stlCSV.Text;
        sProjectWorkIDs := Copy(sProjectWorkIDs, 1, Length(sProjectWorkIDs)-1);
        MarkWorkAsExported(sProjectWorkIDs);
      finally
        stlCSV.Free;
      end;
    end;
  finally
    qryExport.Close;
  end;
end;

procedure TsvmEmployee.MarkWorkAsExported(const ProjectWorkIDs: string);
begin
  cmdMarkProcessed.CommandText.Clear;
  cmdMarkProcessed.CommandText.Add('Update ProjectWork');
  cmdMarkProcessed.CommandText.Add('Set ProcessedStmp = Current_Timestamp');
  cmdMarkProcessed.CommandText.Add(Format('Where ProjectWorkID In (%s)', [ProjectWorkIDs]));
  cmdMarkProcessed.Execute;
end;

end.

