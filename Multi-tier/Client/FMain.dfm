object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'WorkLog - Client/Server'
  ClientHeight = 506
  ClientWidth = 938
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = menMain
  TextHeight = 13
  object pgcMain: TPageControl
    Left = 0
    Top = 0
    Width = 938
    Height = 506
    Align = alClient
    TabOrder = 0
  end
  object menMain: TMainMenu
    Left = 72
    Top = 88
    object menFile: TMenuItem
      Caption = '&File'
      object menEmployee: TMenuItem
        Caption = '&Employee'
        OnClick = menEmployeeClick
      end
      object menProject: TMenuItem
        Caption = '&Project'
        OnClick = menProjectClick
      end
      object menExport: TMenuItem
        Caption = 'Export &Work Hours'
        OnClick = menExportClick
      end
      object menSeparator: TMenuItem
        Caption = '-'
      end
      object menExit: TMenuItem
        Caption = 'E&xit'
        OnClick = menExitClick
      end
    end
  end
  object menPopup: TPopupMenu
    Left = 136
    Top = 88
    object menCloseTabsheet: TMenuItem
      Caption = '&Close'
      OnClick = menCloseTabsheetClick
    end
  end
  object dlgSave: TFileSaveDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'CSV-File'
        FileMask = '*.csv'
      end>
    Options = []
    Left = 72
    Top = 184
  end
end
