inherited svmLookup: TsvmLookup
  OldCreateOrder = True
  Width = 368
  object qryProject: TFDDataSnapQuery
    Connection = conFB
    FetchOptions.AssignedValues = [evMode, evCursorKind]
    FetchOptions.Mode = fmAll
    FetchOptions.CursorKind = ckForwardOnly
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvRefreshMode, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.UpdateChangedFields = False
    UpdateOptions.UpdateMode = upWhereChanged
    UpdateOptions.RefreshMode = rmManual
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'Select ProjectID, Name, UpdNo'
      'From Project'
      'Where Name Like :Name'
      'Order By Name')
    Left = 176
    Top = 40
    ParamData = <
      item
        Name = 'NAME'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end>
    object qryProjectPROJECTID: TIntegerField
      FieldName = 'PROJECTID'
      Origin = 'PROJECTID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qryProjectNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
    end
    object qryProjectUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
    end
  end
  object dspProject: TFDDataSetProvider
    DataSet = qryProject
    Options = [poPropogateChanges]
    UpdateMode = upWhereChanged
    BeforeUpdateRecord = dspProjectBeforeUpdateRecord
    UseSQLForRefreshRecord = True
    Left = 264
    Top = 40
  end
end
