Create Firebird DB in [YourProjectFolder]\Database (e.g. as WorkLog.fdb)
Create a Firebird alias in aliases.conf as WorkLog=[YourProjectFolder]\Database\WorkLog.fdb
Connections to the DB are currently local connections.
The WorkLog.dm2 file is a CaseStudio ERM file.

If you are using Delphi Enterprise or higher and you want to compile the multi-tier project group
you need to build and install the TFDDataSnapQuery & TFDDataSetProvider first. You will find them
in the components folder.