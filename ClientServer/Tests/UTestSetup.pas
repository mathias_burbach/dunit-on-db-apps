unit UTestSetup;

interface

uses
  TestFrameWork,
  TestExtensions;

type
  TWorkLogTestSetup = class(TTestSetup)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  end;

implementation

{ TWorkLogTestSetup }

uses
  DMain,
  DTestHelper,
  UTestDEmployee,
  UTestDLookup,
  UTestFEmployee,
  UTestFLookup;

procedure TWorkLogTestSetup.SetUp;
begin
  inherited;
  dmoMain := TdmoMain.Create(nil);
  dmoTestHelper := TdmoTestHelper.Create(nil);
  dmoTestHelper.CreateFixture;
end;

procedure TWorkLogTestSetup.TearDown;
begin
  dmoTestHelper.Free;
  dmoMain.Free;
  inherited;
end;

initialization
  RegisterTest(
    TWorkLogTestSetup.Create(
      TTestSuite.Create('WorkLog Client/Server Tests',
        [TTestdmoEmployee.Suite,
         TTestdmoLookup.Suite,
         TTestfraEmployee.Suite,
         TTestfraLookup.Suite])));
end.
