Delete From ProjectWork;
Delete From Project;
Delete From Employee;

Insert Into Project (ProjectID, Name, UpdNo) Values (1, 'Project A', 0);
Insert Into Project (ProjectID, Name, UpdNo) Values (2, 'Project B', 0);
Insert Into Project (ProjectID, Name, UpdNo) Values (3, 'Project C', 0);

Insert Into Employee (EmployeeID, FirstName, LastName, UpdNo) Values (1, 'Joe', 'Bloggs', 0);
Insert Into Employee (EmployeeID, FirstName, LastName, UpdNo) Values (2, 'Peter', 'Miller', 0);
Insert Into Employee (EmployeeID, FirstName, LastName, UpdNo) Values (3, 'Tara', 'Smith', 0);

Insert Into ProjectWork (ProjectWorkID, EmployeeID, ProjectID, StartDate, EndDate, Hours, UpdNo) Values (1, 1, 1, '2014-07-14', '2014-07-18', 40.0, 0);
Insert Into ProjectWork (ProjectWorkID, EmployeeID, ProjectID, StartDate, EndDate, Hours, UpdNo) Values (2, 1, 2, '2014-07-20', Null, 15.0, 0);
Insert Into ProjectWork (ProjectWorkID, EmployeeID, ProjectID, StartDate, EndDate, Hours, UpdNo) Values (3, 2, 2, '2014-07-20', Null, 8.0, 0);

Set Generator GenProjectID To 3;
Set Generator GenEmployeeID To 3;
Set Generator GenProjectWorkID To 3;
