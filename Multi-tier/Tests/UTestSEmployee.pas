unit UTestSEmployee;

interface

uses
  UTestBase,
  SEmployee,
  Datasnap.DBClient;

type
  TTestsvmEmployee = class(TBaseTestCase)
  private
    svmEmployee: TsvmEmployee;
    cdsEmployee: TClientDataSet;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestOpen;
    procedure TestAdd;
    procedure TestEdit;
    procedure TestDelete;
    procedure TestOpenWork;
    procedure TestAddWork;
    procedure TestEditWork;
    procedure TestDeleteWork;
    procedure TestExportWorkingHoursToCSV;
  end;


implementation

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  DTestHelper;

{ TTestsvmEmployee }

procedure TTestsvmEmployee.SetUp;
begin
  inherited;
  svmEmployee := TsvmEmployee.Create(nil);
  cdsEmployee := TClientDataSet.Create(nil);
  cdsEmployee.SetProvider(svmEmployee.dspEmployee);
  cdsEmployee.FetchParams;
end;

procedure TTestsvmEmployee.TearDown;
begin
  cdsEmployee.Free;
  svmEmployee.Free;
  inherited;
end;

procedure TTestsvmEmployee.TestAdd;
const
  cLastName = 'Burbach';
  cFirstName = 'Mathias';
  cNextEmployeeID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cFirstName;
  cdsEmployee.Open;
  CheckEquals(0, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsEmployee.Append;
  cdsEmployee.FieldByName('LastName').AsString := cLastName;
  cdsEmployee.FieldByName('FirstName').AsString := cFirstName;
  cdsEmployee.Post;
  cdsEmployee.ApplyUpdates(0);
  CheckEquals(0, cdsEmployee.FieldByName('UpdNo').AsInteger, 'UpdNo');
  CheckEquals(cNextEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cLastName, DBValue, 'LastName');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'FirstName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cFirstName, DBValue, 'FirstName');
end;

procedure TTestsvmEmployee.TestAddWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cEmployeeID = 1;
  cProjectID = 3;
  cNextProjectWorkID = 4;
  cHours = 25.0;
var
  cdsProjectWork: TClientDataSet;
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    CheckEquals(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
    cdsProjectWork.Append;
    cdsProjectWork.FieldByName('StartDate').AsDateTime := Date;
    cdsProjectWork.FieldByName('ProjectID').AsInteger := cProjectID;
    cdsProjectWork.FieldByName('Hours').AsFloat := cHours;
    cdsProjectWork.Post;
    cdsEmployee.ApplyUpdates(0);
    CheckEquals(0, cdsEmployee.ChangeCount, 'ChangeCount');
    cdsProjectWork.Last;
    CheckEquals(cNextProjectWorkID, cdsProjectWork.FieldByName('ProjectWorkID').AsInteger, 'ProjectWorkID');
    CheckEquals(0, cdsProjectWork.FieldByName('UpdNo').AsInteger, 'UpdNo');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'StartDate', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
    CheckEquals(DateToStr(Date), DBValue, 'StartDate');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
    CheckEquals(FloatToStr(cHours), DBValue, 'Hours');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProjectID', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
    CheckEquals(IntToStr(cProjectID), DBValue, 'ProjectID');
  finally
    cdsProjectWork.Free;
  end;
end;

procedure TTestsvmEmployee.TestDelete;
const
  cLastName = 'Smith';
  cAnyValue = '%';
  cEmployeeID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  cdsEmployee.Delete;
  cdsEmployee.ApplyUpdates(0);
  DBValue := dmoTestHelper.GetDBValue('Employee', 'Count(1)', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals('0', DBValue, 'Count');
end;

procedure TTestsvmEmployee.TestDeleteWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cEmployeeID = 1;
  cProjectWorkID = 2;
var
  cdsProjectWork: TClientDataSet;
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    CheckEquals(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
    cdsProjectWork.Last;
    CheckEquals(cProjectWorkID, cdsProjectWork.FieldByName('ProjectWorkID').AsInteger, 'ProjectWorkID');
    cdsProjectWork.Delete;
    cdsEmployee.ApplyUpdates(0);
    CheckEquals(0, cdsEmployee.ChangeCount, 'ChangeCount');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Count(1)', Format('ProjectWorkID=%d', [cProjectWorkID]));
    CheckEquals('0', DBValue, 'Count');
  finally
    cdsProjectWork.Free;
  end;
end;

procedure TTestsvmEmployee.TestEdit;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cChangedName = 'NewLastName';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, cdsEmployee.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  cdsEmployee.Edit;
  cdsEmployee.FieldByName('LastName').AsString := cChangedName;
  cdsEmployee.Post;
  cdsEmployee.ApplyUpdates(0);
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals(cChangedName, DBValue, 'LastName');
end;

procedure TTestsvmEmployee.TestEditWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  cdsProjectWork: TClientDataSet;
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    CheckEquals(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
    cdsProjectWork.Last;
    CheckEquals(cProjectWorkID, cdsProjectWork.FieldByName('ProjectWorkID').AsInteger, 'ProjectWorkID');
    cdsProjectWork.Edit;
    cdsProjectWork.FieldByName('EndDate').AsDateTime := Date;
    cdsProjectWork.FieldByName('Hours').AsFloat := cHours;
    cdsProjectWork.Post;
    cdsEmployee.ApplyUpdates(0);
    CheckEquals(0, cdsEmployee.ChangeCount, 'ChangeCount');
    cdsProjectWork.Last;
    CheckEquals(1, cdsProjectWork.FieldByName('UpdNo').AsInteger, 'UpdNo');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'EndDate', Format('ProjectWorkID=%d', [cProjectWorkID]));
    CheckEquals(DateToStr(Date), DBValue, 'EndDate');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cProjectWorkID]));
    CheckEquals(FloatToStr(cHours), DBValue, 'Hours');
  finally
    cdsProjectWork.Free;
  end;
end;

procedure TTestsvmEmployee.TestExportWorkingHoursToCSV;
const
  caCSV: array[0..1] of string =
    ('EMPLOYEEID,FIRSTNAME,LASTNAME,PROJECT,STARTDATE,ENDDATE,HOURS',
     '1,"Joe","Bloggs","Project A",14/07/2014,18/07/2014,40');
var
  DBValue: string;
  stlCSV: TStringList;
begin
  RunCreateFixtureAtTearDown := True;
  stlCSV := TStringList.Create;
  try
    stlCSV.Text := svmEmployee.ExportWorkingHoursToCSV;
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProcessedStmp', 'ProjectWorkID=1');
    CheckTrue(DBValue <> EmptyStr, 'ProcessedStmp Is Not Null');
    CheckEquals(2, stlCSV.Count, 'CSV.Count');
    CheckEquals(caCSV[0], stlCSV[0], 'stlCSV[0]');
    CheckEquals(caCSV[1], stlCSV[1], 'stlCSV[1]');
  finally
    stlCSV.Free;
  end;
end;

procedure TTestsvmEmployee.TestOpen;
const
  cLastName = 'B%';
  cAnyValue = '%';
begin
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsEmployee.Close;
  // closing the CDS causes the provider to be "lost"
  cdsEmployee.SetProvider(svmEmployee.dspEmployee);
  cdsEmployee.Params.ParamByName('LastName').AsString := cAnyValue;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(3, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
end;

procedure TTestsvmEmployee.TestOpenWork;
const
  cLastName = 'B%';
  cAnyValue = '%';
var
  cdsProjectWork: TClientDataSet;
begin
  cdsEmployee.Params.ParamByName('LastName').AsString := cLastName;
  cdsEmployee.Params.ParamByName('FirstName').AsString := cAnyValue;
  cdsEmployee.Open;
  CheckEquals(1, cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  cdsProjectWork := TClientDataSet.Create(nil);
  try
    cdsProjectWork.DataSetField := cdsEmployee.FieldByName('qryProjectWork') as TDataSetField;
    CheckEquals(2, cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
  finally
    cdsProjectWork.Free;
  end;
end;

end.
