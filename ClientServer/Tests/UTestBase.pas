unit UTestBase;

interface

uses
  TestFramework;

type
  TBaseTestCase = class(TTestCase)
  private
    FRunCreateFixtureAtTearDown: Boolean;
    procedure SetRunCreateFixtureAtTearDown(const Value: Boolean);
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  public
    property RunCreateFixtureAtTearDown: Boolean read FRunCreateFixtureAtTearDown write SetRunCreateFixtureAtTearDown;
  end;

implementation

{ TBaseTestCase }

uses
  DTestHelper;

procedure TBaseTestCase.SetRunCreateFixtureAtTearDown(const Value: Boolean);
begin
  FRunCreateFixtureAtTearDown := Value;
end;

procedure TBaseTestCase.SetUp;
begin
  inherited;
  FRunCreateFixtureAtTearDown := False;
end;

procedure TBaseTestCase.TearDown;
begin
  if FRunCreateFixtureAtTearDown then
  begin
    dmoTestHelper.CreateFixture;
    FRunCreateFixtureAtTearDown := False;
  end;
  inherited;
end;

end.
