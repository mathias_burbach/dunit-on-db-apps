unit DTestHelper;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Comp.ScriptCommands,
  FireDAC.Stan.Util,
  FireDAC.Comp.Script,
  FireDAC.Comp.Client,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI,
  FireDAC.Phys.FBDef;

type
  TdmoTestHelper = class(TDataModule)
    conFB: TFDConnection;
    scrCreateFixture: TFDScript;
    qryTask: TFDQuery;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateFixture;
     function GetDBValue(const TableName, ColumnName, WhereClause: string): string;
     procedure ChangeDBRecord(const TableName, SetClause, WhereClause: string);
  end;

var
  dmoTestHelper: TdmoTestHelper;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoTestHelper }

procedure TdmoTestHelper.ChangeDBRecord(const TableName, SetClause, WhereClause: string);
begin
  qryTask.SQL.Clear;
  qryTask.SQL.Add('Update ' + TableName);
  qryTask.SQL.Add('Set ' + SetClause);
  if WhereClause <> EmptyStr then
    qryTask.SQL.Add('Where ' + WhereClause);
  qryTask.ExecSQL;
end;

procedure TdmoTestHelper.CreateFixture;
begin
  conFB.StartTransaction;
  try
    scrCreateFixture.ExecuteAll;
    conFB.Commit;
  except
    conFB.Rollback;
    raise;
  end;
end;

procedure TdmoTestHelper.DataModuleCreate(Sender: TObject);
begin
  conFB.Open;
end;

function TdmoTestHelper.GetDBValue(const TableName, ColumnName, WhereClause: string): string;
begin
  Result := '[Not Found]';
  qryTask.SQL.Clear;
  qryTask.SQL.Add('Select ' + ColumnName);
  qryTask.SQL.Add('From ' + TableName);
  if WhereClause <> EmptyStr then
    qryTask.SQL.Add('Where ' + WhereClause);
  qryTask.Open;
  try
    if not qryTask.Eof then
      Result := qryTask.Fields[0].AsString;
  finally
    qryTask.Close;
  end;
end;

end.
