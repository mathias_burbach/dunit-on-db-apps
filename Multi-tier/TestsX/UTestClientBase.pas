unit UTestClientBase;

interface

uses
  UTestBase;

type
  TBaseClientTestCase = class(TBaseTestCase)
  private
  public
    procedure SetupFixture;
    procedure TearDownFixture;
  end;

implementation

{ TBaseTestCase }

uses DMain;

procedure TBaseClientTestCase.SetupFixture;
begin
  dmoMain := TdmoMain.Create(nil);
end;

procedure TBaseClientTestCase.TearDownFixture;
begin
  dmoMain.Free;
end;

end.
