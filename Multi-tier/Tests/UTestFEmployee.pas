unit UTestFEmployee;

interface

uses
  UTestBaseGUI,
  FEmployeeFrame;

type
  TTestfraEmployee = class(TBaseGUITestCase)
  private
    fraEmployee: TfraEmployee;
  protected
    procedure SetUp; override;
  published
    procedure TestOpen;
    procedure TestAdd;
    procedure TestEdit;
    procedure TestDelete;
  end;

implementation

uses
  System.SysUtils,
  Vcl.Controls,
  Vcl.DBCtrls,
  DTestHelper;

{ TTestfraEmployee }

procedure TTestfraEmployee.SetUp;
begin
  inherited;
  fraEmployee := TfraEmployee.Create(frmTestGUI);
  fraEmployee.Parent := frmTestGUI;
  fraEmployee.Align := alClient;
  fraEmployee.InitialiseDataAccess;
  fraEmployee.SetFocus;
end;

procedure TTestfraEmployee.TestAdd;
const
  cLastName = 'Burbach';
  cFirstName = 'Mathias';
  cNextEmployeeID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  EnterText(cFirstName);
  Tab;
  EnterText(cLastName);
  Tab;
  Click;
  CheckEquals(0, fraEmployee.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  SetFocus(fraEmployee.dbeFirstName);
  fraEmployee.navMain.BtnClick(nbInsert);
  EnterText(cFirstName);
  Tab;
  EnterText(cLastName);
  Tab;
  fraEmployee.navMain.BtnClick(nbPost);
  CheckEnabled(fraEmployee.btnApplyUpdates);
  fraEmployee.btnApplyUpdates.Click;
  CheckFalse(fraEmployee.btnApplyUpdates.Enabled, 'btnApplyUpdates.Enabled');
  CheckEquals(0, fraEmployee.dscMain.DataSet.FieldByName('UpdNo').AsInteger, 'UpdNo');
  CheckEquals(cNextEmployeeID, fraEmployee.dscMain.DataSet.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cLastName, DBValue, 'LastName');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'FirstName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cFirstName, DBValue, 'FirstName');
end;

procedure TTestfraEmployee.TestDelete;
const
  cLastName = 'Smith';
  cEmployeeID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  Tab;
  EnterText(cLastName);
  Tab;
  Click;
  CheckEquals(1, fraEmployee.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  CheckEquals(cEmployeeID, fraEmployee.dscMain.DataSet.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  CheckTrue(fraEmployee.navMain.ConfirmDelete, 'ConfirmDelete');
  fraEmployee.navMain.ConfirmDelete := False;
  fraEmployee.navMain.BtnClick(nbDelete);
  CheckEnabled(fraEmployee.btnApplyUpdates);
  fraEmployee.btnApplyUpdates.Click;
  CheckFalse(fraEmployee.btnApplyUpdates.Enabled, 'btnApplyUpdates.Enabled');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'Count(1)', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals('0', DBValue, 'LastName');
end;

procedure TTestfraEmployee.TestEdit;
const
  cLastName = 'B';
  cChangedName = 'NewLastName';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  Tab;
  EnterText(cLastName);
  Tab;
  Click;
  CheckEquals(1, fraEmployee.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  CheckEquals(cEmployeeID, fraEmployee.dscMain.DataSet.FieldByName('EmployeeID').AsInteger, 'EmployeeID');
  SetFocus(fraEmployee.dbeLastName);
  fraEmployee.navMain.BtnClick(nbEdit);
  EnterText(cChangedName);
  Tab;
  fraEmployee.navMain.BtnClick(nbPost);
  CheckEnabled(fraEmployee.btnApplyUpdates);
  fraEmployee.btnApplyUpdates.Click;
  CheckFalse(fraEmployee.btnApplyUpdates.Enabled, 'btnApplyUpdates.Enabled');
  CheckEquals(1, fraEmployee.dscMain.DataSet.FieldByName('UpdNo').AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals(cChangedName, DBValue, 'LastName');
end;

procedure TTestfraEmployee.TestOpen;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  Tab;
  EnterText(cLastName);
  Tab;
  Click;
  CheckEquals(1, fraEmployee.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
  Tab(-1);
  EnterText(cAnyValue);
  Tab;
  Click;
  CheckEquals(3, fraEmployee.dscMain.DataSet.RecordCount, 'dscMain.DataSet.RecordCount');
end;

end.
