inherited dmoEmployee: TdmoEmployee
  Height = 268
  Width = 433
  object dpcEmployee: TDSProviderConnection
    ServerClassName = 'TsvmEmployee'
    SQLConnection = dmoMain.conAppServer
    Left = 64
    Top = 56
  end
  object cdsEmployee: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'LASTNAME'
        ParamType = ptInput
        Size = 20
      end
      item
        DataType = ftString
        Name = 'FIRSTNAME'
        ParamType = ptInput
        Size = 20
      end>
    ProviderName = 'dspEmployee'
    RemoteServer = dpcEmployee
    OnNewRecord = cdsEmployeeNewRecord
    Left = 160
    Top = 56
    object cdsEmployeeEMPLOYEEID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsEmployeeFIRSTNAME: TStringField
      DisplayLabel = 'First Name'
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
    end
    object cdsEmployeeLASTNAME: TStringField
      DisplayLabel = 'Last Name'
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
    end
    object cdsEmployeeUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
      Visible = False
    end
    object cdsEmployeeqryProjectWork: TDataSetField
      FieldName = 'qryProjectWork'
      Visible = False
    end
  end
  object cdsProjectWork: TClientDataSet
    Aggregates = <>
    DataSetField = cdsEmployeeqryProjectWork
    Params = <>
    OnNewRecord = cdsProjectWorkNewRecord
    Left = 256
    Top = 56
    object cdsProjectWorkPROJECTWORKID: TIntegerField
      FieldName = 'PROJECTWORKID'
      Visible = False
    end
    object cdsProjectWorkEMPLOYEEID: TIntegerField
      FieldName = 'EMPLOYEEID'
      Required = True
      Visible = False
    end
    object cdsProjectWorkPROJECTID: TIntegerField
      FieldName = 'PROJECTID'
      Required = True
      Visible = False
    end
    object cdsProjectWorkProject: TStringField
      FieldKind = fkLookup
      FieldName = 'Project'
      LookupDataSet = cdsProjectLU
      LookupKeyFields = 'PROJECTID'
      LookupResultField = 'NAME'
      KeyFields = 'PROJECTID'
      Size = 35
      Lookup = True
    end
    object cdsProjectWorkSTARTDATE: TDateField
      DisplayLabel = 'Start Date'
      FieldName = 'STARTDATE'
      Required = True
    end
    object cdsProjectWorkENDDATE: TDateField
      DisplayLabel = 'End Date'
      FieldName = 'ENDDATE'
    end
    object cdsProjectWorkHOURS: TFloatField
      DisplayLabel = 'Hours'
      FieldName = 'HOURS'
    end
    object cdsProjectWorkPROCESSEDSTMP: TSQLTimeStampField
      FieldName = 'PROCESSEDSTMP'
      Visible = False
    end
    object cdsProjectWorkUPDNO: TIntegerField
      FieldName = 'UPDNO'
      Visible = False
    end
  end
  object dpcLookup: TDSProviderConnection
    ServerClassName = 'TsvmLookup'
    SQLConnection = dmoMain.conAppServer
    Left = 64
    Top = 168
  end
  object cdsProjectLU: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'NAME'
        ParamType = ptInput
        Size = 20
        Value = '%'
      end>
    ProviderName = 'dspProject'
    RemoteServer = dpcLookup
    Left = 160
    Top = 168
  end
  object ssmExportWorkingHoursToCSV: TSqlServerMethod
    Params = <
      item
        DataType = ftWideString
        Precision = 2000
        Name = 'ReturnParameter'
        ParamType = ptResult
        Size = 2000
      end>
    SQLConnection = dmoMain.conAppServer
    ServerMethodName = 'TsvmEmployee.ExportWorkingHoursToCSV'
    Left = 296
    Top = 168
  end
end
