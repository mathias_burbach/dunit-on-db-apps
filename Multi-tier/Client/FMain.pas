unit FMain;

interface

uses
  System.SysUtils,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.Menus,
  Vcl.ComCtrls;

type
  TfrmMain = class(TForm)
    menMain: TMainMenu;
    menFile: TMenuItem;
    menEmployee: TMenuItem;
    menProject: TMenuItem;
    menSeparator: TMenuItem;
    menExit: TMenuItem;
    pgcMain: TPageControl;
    menPopup: TPopupMenu;
    menCloseTabsheet: TMenuItem;
    dlgSave: TFileSaveDialog;
    menExport: TMenuItem;
    procedure menExitClick(Sender: TObject);
    procedure menEmployeeClick(Sender: TObject);
    procedure menCloseTabsheetClick(Sender: TObject);
    procedure menProjectClick(Sender: TObject);
    procedure menExportClick(Sender: TObject);
  private
    { Private declarations }
    FPageCounter: Integer;
    function AddTabSheet: TTabSheet;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  Winapi.Windows,
  Winapi.Messages,
  Winapi.ShellAPI,
  System.IOUtils,
  FEmployeeFrame,
  FLookupFrame,
  DEmployee;

function TfrmMain.AddTabSheet: TTabSheet;
begin
  Result := TTabSheet.Create(pgcMain);
  Inc(FPageCounter);
  Result.Name := Format('tab%d', [FPageCounter]);
  Result.PageControl := pgcMain;
  pgcMain.ActivePage := Result;
end;

procedure TfrmMain.menCloseTabsheetClick(Sender: TObject);
begin
  pgcMain.ActivePage.Free;
end;

procedure TfrmMain.menEmployeeClick(Sender: TObject);
var
  Page: TTabSheet;
  fraEmployee: TfraEmployee;
begin
  Page := AddTabSheet;
  Page.Caption := 'Employee';
  Page.PopupMenu := menPopup;
  fraEmployee := TfraEmployee.Create(Self);
  fraEmployee.Name := fraEmployee.Name + IntToStr(FPageCounter);
  fraEmployee.InitialiseDataAccess;
  fraEmployee.Parent := Page;
  fraEmployee.Align := alClient;
  fraEmployee.SetFocus;
end;

procedure TfrmMain.menExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.menExportClick(Sender: TObject);
var
  dmoEmployee: TdmoEmployee;
begin
  if dlgSave.Execute then
  begin
    dmoEmployee := TdmoEmployee.Create(Self);
    try
      //dmoEmployee.ExportWorkingHoursToCSV(dlgSave.FileName);
    finally
      dmoEmployee.Free;
    end;
    if TFile.Exists(dlgSave.FileName) then
      ShellExecute(0, 'OPEN', PChar(dlgSave.FileName), '', '', SW_SHOWNORMAL);
  end;
end;

procedure TfrmMain.menProjectClick(Sender: TObject);
var
  Page: TTabSheet;
  fraLookup: TfraLookup;
begin
  Page := AddTabSheet;
  Page.Caption := 'Project';
  Page.PopupMenu := menPopup;
  fraLookup := TfraLookup.Create(Self, 'Project');
  fraLookup.Name := fraLookup.Name + IntToStr(FPageCounter);
  fraLookup.InitialiseDataAccess;
  fraLookup.Parent := Page;
  fraLookup.Align := alClient;
  fraLookup.SetFocus;
end;

end.
