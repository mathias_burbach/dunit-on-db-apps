unit UTestDEmployee;

interface

uses
  UTestBase,
  DEmployee;

type
  TTestdmoEmployee = class(TBaseTestCase)
  private
    dmoEmployee: TdmoEmployee;
    FApplyUpdateErrorTriggered: Boolean;
    procedure DoApplyUpdateError(const ErrorMessage: string);
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestOpen;
    procedure TestAdd;
    procedure TestEdit;
    procedure TestEditChangedEmployee;
    procedure TestDelete;
    procedure TestOpenWork;
    procedure TestAddWork;
    procedure TestEditWork;
    procedure TestDeleteWork;
    procedure TestExportWorkingHoursToCSV;
  end;

implementation

uses
  System.SysUtils,
  System.Classes,
  DTestHelper;

{ TTestdmoEmployee }

procedure TTestdmoEmployee.DoApplyUpdateError(const ErrorMessage: string);
begin
  FApplyUpdateErrorTriggered := True;
end;

procedure TTestdmoEmployee.SetUp;
begin
  inherited;
  dmoEmployee := TdmoEmployee.Create(nil);
end;

procedure TTestdmoEmployee.TearDown;
begin
  dmoEmployee.Free;
  inherited;
end;

procedure TTestdmoEmployee.TestAdd;
const
  cLastName = 'Burbach';
  cFirstName = 'Mathias';
  cNextEmployeeID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, cFirstName);
  CheckEquals(0, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  dmoEmployee.cdsEmployee.Append;
  dmoEmployee.cdsEmployeeLASTNAME.AsString := cLastName;
  dmoEmployee.cdsEmployeeFIRSTNAME.AsString := cFirstName;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  CheckEquals(0, dmoEmployee.cdsEmployeeUPDNO.AsInteger, 'UpdNo');
  CheckEquals(cNextEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cLastName, DBValue, 'LastName');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'FirstName', Format('EmployeeID=%d', [cNextEmployeeID]));
  CheckEquals(cFirstName, DBValue, 'FirstName');
end;

procedure TTestdmoEmployee.TestAddWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectID = 3;
  cNextProjectWorkID = 4;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsProjectWork.Last;
  CheckEquals(2, dmoEmployee.cdsProjectWork.RecordCount, 'qryWork.RecordCount');
  dmoEmployee.cdsProjectWork.Append;
  CheckEquals(Date, dmoEmployee.cdsProjectWorkSTARTDATE.AsDateTime, 'StartDate');
  dmoEmployee.cdsProjectWorkPROJECTID.AsInteger := cProjectID;
  dmoEmployee.cdsProjectWorkHOURS.AsFloat := cHours;
  dmoEmployee.cdsProjectWork.Post;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  dmoEmployee.cdsProjectWork.Last;
  CheckEquals(cNextProjectWorkID, dmoEmployee.cdsProjectWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  CheckEquals(0, dmoEmployee.cdsProjectWorkUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'StartDate', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  CheckEquals(DateToStr(Date), DBValue, 'StartDate');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  CheckEquals(FloatToStr(cHours), DBValue, 'Hours');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProjectID', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  CheckEquals(IntToStr(cProjectID), DBValue, 'ProjectID');
end;

procedure TTestdmoEmployee.TestDelete;
const
  cLastName = 'Smith';
  cEmployeeID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsEmployee.Delete;
  dmoEmployee.ApplyUpdates;
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'Count(1)', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals('0', DBValue, 'LastName');
end;

procedure TTestdmoEmployee.TestDeleteWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsProjectWork.Last;
  CheckEquals(2, dmoEmployee.cdsProjectWork.RecordCount, 'qryWork.RecordCount');
  CheckEquals(cProjectWorkID, dmoEmployee.cdsProjectWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  dmoEmployee.cdsProjectWork.Delete;
  dmoEmployee.ApplyUpdates;
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Count(1)', Format('ProjectWorkID=%d', [cProjectWorkID]));
  CheckEquals('0', DBValue, 'Count');
end;

procedure TTestdmoEmployee.TestEdit;
const
  cLastName = 'B';
  cChangedName = 'NewLastName';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsEmployee.Edit;
  dmoEmployee.cdsEmployeeLASTNAME.AsString := cChangedName;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  CheckEquals(1, dmoEmployee.cdsEmployeeUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cEmployeeID]));
  CheckEquals(cChangedName, DBValue, 'LastName');
end;

procedure TTestdmoEmployee.TestEditChangedEmployee;
const
  cLastName = 'B';
  cChangedName = 'NewLastName';
  cOtherUserChange = 'Other';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoTestHelper.ChangeDBRecord('Employee', Format('FirstName=''%s'', UpdNo=UpdNo+1', [cOtherUserChange]), Format('EmployeeID=%d', [cEmployeeID]));
  dmoEmployee.OnApplyUpdateError := DoApplyUpdateError;
  FApplyUpdateErrorTriggered := False;
  dmoEmployee.cdsEmployee.Edit;
  dmoEmployee.cdsEmployeeLASTNAME.AsString := cChangedName;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  CheckTrue(FApplyUpdateErrorTriggered, 'FApplyUpdateErrorTriggered');
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  CheckEquals(cOtherUserChange, dmoEmployee.cdsEmployeeFIRSTNAME.AsString, 'FirstName');
  CheckEquals(1, dmoEmployee.cdsEmployeeUPDNO.AsInteger, 'UpdNo');
end;

procedure TTestdmoEmployee.TestEditWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsProjectWork.Last;
  CheckEquals(2, dmoEmployee.cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
  CheckEquals(cProjectWorkID, dmoEmployee.cdsProjectWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  dmoEmployee.cdsProjectWork.Edit;
  dmoEmployee.cdsProjectWorkENDDATE.AsDateTime := Date;
  dmoEmployee.cdsProjectWorkHOURS.AsFloat := cHours;
  dmoEmployee.cdsProjectWork.Post;
  dmoEmployee.ApplyUpdates;
  CheckFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  CheckEquals(1, dmoEmployee.cdsProjectWorkUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'EndDate', Format('ProjectWorkID=%d', [cProjectWorkID]));
  CheckEquals(DateToStr(Date), DBValue, 'EndDate');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cProjectWorkID]));
  CheckEquals(FloatToStr(cHours), DBValue, 'Hours');
end;

procedure TTestdmoEmployee.TestExportWorkingHoursToCSV;
const
  cFileName = 'Export.csv';
  caCSV: array[0..1] of string =
    ('EMPLOYEEID,FIRSTNAME,LASTNAME,PROJECT,STARTDATE,ENDDATE,HOURS',
     '1,"Joe","Bloggs","Project A",14/07/2014,18/07/2014,40');
var
  DBValue: string;
  stlCSV: TStringList;
begin
  RunCreateFixtureAtTearDown := True;
  stlCSV := TStringList.Create;
  try
    stlCSV.Text := dmoEmployee.ExportWorkingHoursToCSV;
    CheckEquals(2, stlCSV.Count, 'CSV.Count');
    CheckEquals(caCSV[0], stlCSV[0], 'stlCSV[0]');
    CheckEquals(caCSV[1], stlCSV[1], 'stlCSV[1]');
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProcessedStmp', 'ProjectWorkID=1');
    CheckTrue(DBValue <> EmptyStr, 'ProcessedStmp Is Not Null');
  finally
    stlCSV.Free;
  end;
end;

procedure TTestdmoEmployee.TestOpen;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  dmoEmployee.Open(cAnyValue, EmptyStr);
  CheckEquals(3, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
end;

procedure TTestdmoEmployee.TestOpenWork;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  dmoEmployee.Open(cLastName, EmptyStr);
  CheckEquals(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  CheckEquals(2, dmoEmployee.cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
end;

end.
