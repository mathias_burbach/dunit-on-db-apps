unit DBase;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.DBClient;

type
  TApplyUpdateErrorEvent = procedure(const ErrorMessage: string) of object;

  TdmoBase = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FOnApplyUpdateError: TApplyUpdateErrorEvent;
    FMainCDS: TClientDataSet;
    procedure SetOnApplyUpdateError(const Value: TApplyUpdateErrorEvent);
    procedure SetMainCDS(const Value: TClientDataSet);
    procedure cdsMainReconcileError(DataSet: TCustomClientDataSet;
                                    E: EReconcileError;
                                    UpdateKind: TUpdateKind;
                                    var Action: TReconcileAction);
  protected
    { Protected declarations }
    FNextTemporaryPK: Integer;
    function GetNextTemporaryPK: Integer;
  public
    { Public declarations }
    function HasPendingChanges: Boolean;
    procedure ApplyUpdates;
    property MainCDS: TClientDataSet read FMainCDS write SetMainCDS;
    property OnApplyUpdateError: TApplyUpdateErrorEvent read FOnApplyUpdateError write SetOnApplyUpdateError;
  end;

implementation

const
  cCRLF = Chr(13) + Chr(10);

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoBase }

procedure TdmoBase.ApplyUpdates;
begin
  if Assigned(FMainCDS) then
  begin
    if FMainCDS.ChangeCount > 0 then
    begin
      FMainCDS.ApplyUpdates(0);
      if FMainCDS.ChangeCount > 0 then
      begin
        FMainCDS.CancelUpdates;
        FMainCDS.RefreshRecord;
      end;
    end;  end;
end;

procedure TdmoBase.cdsMainReconcileError(DataSet: TCustomClientDataSet;
                                         E: EReconcileError;
                                         UpdateKind: TUpdateKind;
                                         var Action: TReconcileAction);
begin
  if Assigned(OnApplyUpdateError) then
    FOnApplyUpdateError(E.Message + cCRLF +
                        'Will refresh from database');
  Action := TReconcileAction.raAbort;
end;

procedure TdmoBase.DataModuleCreate(Sender: TObject);
begin
  FNextTemporaryPK := 0;
end;

function TdmoBase.GetNextTemporaryPK: Integer;
begin
  Dec(FNextTemporaryPK);
  Result := FNextTemporaryPK;
end;

function TdmoBase.HasPendingChanges: Boolean;
begin
  Result := False;
  if Assigned(FMainCDS) then
    Result := FMainCDS.ChangeCount > 0;
end;

procedure TdmoBase.SetMainCDS(const Value: TClientDataSet);
begin
  FMainCDS := Value;
  if Assigned(FMainCDS) then
    FMainCDS.OnReconcileError := cdsMainReconcileError;
end;

procedure TdmoBase.SetOnApplyUpdateError(const Value: TApplyUpdateErrorEvent);
begin
  FOnApplyUpdateError := Value;
end;

end.
