unit UTestBaseGUI;

interface

uses
  GUITesting,
  FTestGUI;

type
  TBaseGUITestCase = class(TGUITestCase)
  private
    FRunCreateFixtureAtTearDown: Boolean;
    procedure SetRunCreateFixtureAtTearDown(const Value: Boolean);
  protected
    frmTestGUI: TfrmTestGUI;
    procedure SetUp; override;
  public
    procedure TearDown; override;
    property RunCreateFixtureAtTearDown: Boolean read FRunCreateFixtureAtTearDown write SetRunCreateFixtureAtTearDown;
  end;


implementation

uses
  DTestHelper;

{ TBaseGUITestCase }

procedure TBaseGUITestCase.SetRunCreateFixtureAtTearDown(const Value: Boolean);
begin
  FRunCreateFixtureAtTearDown := Value;
end;

procedure TBaseGUITestCase.SetUp;
begin
  inherited;
  frmTestGUI := TfrmTestGUI.Create(nil);
  GUI := frmTestGUI;
  GUI.Show;
end;

procedure TBaseGUITestCase.TearDown;
begin
  if FRunCreateFixtureAtTearDown then
  begin
    dmoTestHelper.CreateFixture;
    FRunCreateFixtureAtTearDown := False;
  end;
  inherited;
end;

end.
