unit SBase;

interface

uses
  System.Classes,
  Data.DB,
  Datasnap.DSProviderDataModuleAdapter,
  Datasnap.DBClient,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Comp.Client,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait;

type
  TsvmBase = class(TDSServerModule)
    conFB: TFDConnection;
    qryGetNextPKValue: TFDQuery;
  private
    { Private declarations }
  protected
    { Protected declarations }
    FTableName: string;
    function GetNextPKValue(const TableName: string): Integer;
    procedure SetPKAndUpdNo(const SourceDS: TDataSet;
                            const DeltaDS: TCustomClientDataSet;
                            const UpdateKind: TUpdateKind;
                            out NewPK: Integer);
  public
    { Public declarations }
  end;

implementation

uses
  System.SysUtils,
  System.StrUtils,
  UFDDataSnapQuery;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TsvmBase }

function TsvmBase.GetNextPKValue(const TableName: string): Integer;
begin
  qryGetNextPKValue.SQL[0] := Format('Select Gen_ID(Gen%sID, 1)', [TableName]);
  qryGetNextPKValue.Open;
  try
    Result := qryGetNextPKValue.Fields[0].AsInteger;
  finally
    qryGetNextPKValue.Close;
  end;
end;

procedure TsvmBase.SetPKAndUpdNo(const SourceDS: TDataSet;
                                 const DeltaDS: TCustomClientDataSet;
                                 const UpdateKind: TUpdateKind;
                                 out NewPK: Integer);
var
  sTableName: string;
  fld: TField;
begin
  NewPK := -1;
  if UpdateKind = ukInsert then
  begin
    sTableName := Copy(SourceDS.Name, 4, Length(SourceDS.Name) - 3);
    fld := DeltaDS.FieldByName(sTableName + 'ID');
    NewPK := GetNextPKValue(sTableName);
    fld.NewValue := NewPK;
    fld := DeltaDS.FieldByName('UpdNo');
    fld.NewValue := 0;
  end
  else if UpdateKind = ukModify then
  begin
    fld := DeltaDS.FieldByName('UpdNo');
    fld.NewValue := fld.OldValue + 1;
  end;
end;

end.

