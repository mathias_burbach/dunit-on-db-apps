unit UFDDataSnapQuery;

interface

uses
  System.Classes,
  Data.DB,
  FireDAC.Comp.Client;

type
  TFDDataSnapQuery = class(TFDQuery)
  protected
    {$IF CompilerVersion < 28} // XE6 and before
    function PSUpdateRecord(AUpdateKind: TUpdateKind;
                            ADelta: TDataSet): Boolean; override;
    {$ENDIF}
  public
    constructor Create(AOwner: TComponent); override;
  end;

procedure Register;

implementation

uses
  FireDAC.Stan.Option;

procedure Register;
begin
  RegisterComponents('FD-DataSnap', [TFDDataSnapQuery]);
end;

{ TFDDataSnapQuery }

constructor TFDDataSnapQuery.Create(AOwner: TComponent);
begin
  inherited;
  UpdateOptions.FastUpdates := True;
  UpdateOptions.UpdateMode := upWhereChanged;
  FetchOptions.CursorKind := ckForwardOnly;
  FetchOptions.Mode := fmAll;
end;

{$IF CompilerVersion < 28}
function TFDDataSnapQuery.PSUpdateRecord(AUpdateKind: TUpdateKind;
                                         ADelta: TDataSet): Boolean;
begin
  Result := False;
end;
{$ENDIF}

end.
