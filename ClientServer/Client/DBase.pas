unit DBase;

interface

uses
  System.Classes,
  Data.DB;

type
  TdmoBase = class(TDataModule)
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure DoBeforePost(const DataSet: TDataSet);
  public
    { Public declarations }
  end;

var
  dmoBase: TdmoBase;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmoBase }

procedure TdmoBase.DoBeforePost(const DataSet: TDataSet);
var
  fld: TField;
begin
  fld := DataSet.FieldByName('UpdNo');
  if DataSet.State = dsInsert then
    fld.AsInteger := 0
  else
    fld.AsInteger := fld.AsInteger + 1;
end;

end.
