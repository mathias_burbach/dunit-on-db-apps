unit DLookup;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Stan.Async,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  DBase;

type
  TdmoLookup = class(TdmoBase)
    qryLookup: TFDQuery;
    procedure qryLookupBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FTableName, FName: string;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent;
                       const TableName: string); reintroduce;
    procedure Open(const Name: string);
  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses DMain;

{$R *.dfm}

{ TdmoLookup }

constructor TdmoLookup.Create(AOwner: TComponent; const TableName: string);
begin
  inherited Create(AOwner);
  FTableName := TableName;
  FName := '???';
end;

procedure TdmoLookup.Open(const Name: string);
begin
  if FName <> Name then
  begin
    qryLookup.DisableControls;
    try
      qryLookup.Close;
      try
        qryLookup.SQL.Clear;
        qryLookup.SQL.Add('Select *');
        qryLookup.SQL.Add('From ' + FTableName);
        if Name = EmptyStr then
          qryLookup.SQL.Add('Where 1 = 2')
        else
          qryLookup.SQL.Add(Format('Where Name Like ''%s''', [Name + '%']));
        qryLookup.SQL.Add('Order By Name');
      finally
        qryLookup.Open;
      end;
      FName := Name;
      qryLookup.UpdateOptions.GeneratorName := Format('GEN%sID', [UpperCase(FTableName)]);
      qryLookup.UpdateOptions.KeyFields := Format('%sID', [UpperCase(FTableName)]);
      qryLookup.Fields[0].DisplayLabel := 'ID';
      qryLookup.Fields[0].AutoGenerateValue := arAutoInc;
      qryLookup.Fields[0].Required := False;
      qryLookup.Fields[0].ReadOnly := True;
      qryLookup.Fields[1].DisplayLabel := 'Name';
      qryLookup.Fields[2].Visible := False;
    finally
      qryLookup.EnableControls;
    end;
  end;
end;

procedure TdmoLookup.qryLookupBeforePost(DataSet: TDataSet);
begin
  inherited;
  DoBeforePost(DataSet);
end;

end.
