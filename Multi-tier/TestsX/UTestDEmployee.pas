unit UTestDEmployee;

interface

uses
  DUnitX.TestFramework,
  Winapi.ShellApi,
  UTestClientBase,
  DEmployee;

type
  [Category('ClientTests')]
  TTestdmoEmployee = class(TBaseClientTestCase)
  strict private
    dmoEmployee: TdmoEmployee;
    FApplyUpdateErrorTriggered: Boolean;
    FAppServerStarted: Boolean;
    FShellInfo: TShellExecuteInfo;
    procedure StartAppServer;
    procedure StopAppServer;
    procedure DoApplyUpdateError(const ErrorMessage: string);
  public
    [SetupFixture]
    procedure SetupFixture;
    [TearDownFixture]
    procedure TearDownFixture;
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure TestOpen;
    [Test]
    procedure TestAdd;
    [Test]
    procedure TestEdit;
    [Test]
    procedure TestEditChangedEmployee;
    [Test]
    procedure TestDelete;
    [Test]
    procedure TestOpenWork;
    [Test]
    procedure TestAddWork;
    [Test]
    procedure TestEditWork;
    [Test]
    procedure TestDeleteWork;
    [Test]
    procedure TestExportWorkingHoursToCSV;
  end;

implementation

uses
  System.SysUtils,
  System.Classes,
  Vcl.Forms,
  Winapi.Windows,
  DTestHelper;

{ TTestdmoEmployee }

procedure TTestdmoEmployee.DoApplyUpdateError(const ErrorMessage: string);
begin
  FApplyUpdateErrorTriggered := True;
end;

procedure TTestdmoEmployee.Setup;
begin
  inherited Setup;
  dmoEmployee := TdmoEmployee.Create(nil);
end;

procedure TTestdmoEmployee.SetupFixture;
begin
  StartAppServer;
  inherited SetupFixture;
end;

procedure TTestdmoEmployee.StartAppServer;
var
  sFileName: string;
begin
  sFileName := ExpandFileName(ExtractFilePath(ParamStr(0)) +'..\..\..\Server\Win32\Debug\WorkLogServer.exe');
  FillChar(FShellInfo, SizeOf(TShellExecuteInfo), 0);
  FShellInfo.cbSize := SizeOf(TShellExecuteInfo);
  FShellInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI or SEE_MASK_FLAG_DDEWAIT;
  FShellInfo.Wnd := Application.Handle;
  FShellInfo.lpVerb := PChar('open');
  FShellInfo.lpFile := PChar(sFileName);
  FShellInfo.lpParameters := nil;
  FShellInfo.lpDirectory := PChar(ExtractFilePath(sFileName));
  FShellInfo.nShow := SW_SHOWMINIMIZED;
  FAppServerStarted := ShellExecuteEx(@FShellInfo);
end;

procedure TTestdmoEmployee.StopAppServer;
begin
  if FAppServerStarted then
    TerminateProcess(FShellInfo.hProcess, 0);
end;

procedure TTestdmoEmployee.TearDown;
begin
  dmoEmployee.Free;
  inherited TearDown;
end;

procedure TTestdmoEmployee.TearDownFixture;
begin
  inherited TearDownFixture;
  StopAppServer;
end;

procedure TTestdmoEmployee.TestAdd;
const
  cLastName = 'Burbach';
  cFirstName = 'Mathias';
  cNextEmployeeID = 4;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, cFirstName);
  Assert.AreEqual(0, dmoEmployee.cdsEmployee.RecordCount, 'qryEmployee.RecordCount');
  dmoEmployee.cdsEmployee.Append;
  dmoEmployee.cdsEmployeeLASTNAME.AsString := cLastName;
  dmoEmployee.cdsEmployeeFIRSTNAME.AsString := cFirstName;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  Assert.AreEqual(0, dmoEmployee.cdsEmployeeUPDNO.AsInteger, 'UpdNo');
  Assert.AreEqual(cNextEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cNextEmployeeID]));
  Assert.AreEqual(cLastName, DBValue, 'LastName');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'FirstName', Format('EmployeeID=%d', [cNextEmployeeID]));
  Assert.AreEqual(cFirstName, DBValue, 'FirstName');
end;

procedure TTestdmoEmployee.TestAddWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectID = 3;
  cNextProjectWorkID = 4;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsProjectWork.Last;
  Assert.AreEqual(2, dmoEmployee.cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
  dmoEmployee.cdsProjectWork.Append;
  Assert.AreEqual(Date, dmoEmployee.cdsProjectWorkSTARTDATE.AsDateTime, 'StartDate');
  dmoEmployee.cdsProjectWorkPROJECTID.AsInteger := cProjectID;
  dmoEmployee.cdsProjectWorkHOURS.AsFloat := cHours;
  dmoEmployee.cdsProjectWork.Post;
  dmoEmployee.ApplyUpdates;
  Assert.AreEqual<Int64>(0, dmoEmployee.cdsEmployee.ChangeCount, 'ChangeCount');
  dmoEmployee.cdsProjectWork.Last;
  Assert.AreEqual(cNextProjectWorkID, dmoEmployee.cdsProjectWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  Assert.AreEqual(0, dmoEmployee.cdsProjectWorkUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'StartDate', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  Assert.AreEqual(DateToStr(Date), DBValue, 'StartDate');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  Assert.AreEqual(FloatToStr(cHours), DBValue, 'Hours');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProjectID', Format('ProjectWorkID=%d', [cNextProjectWorkID]));
  Assert.AreEqual(IntToStr(cProjectID), DBValue, 'ProjectID');
end;

procedure TTestdmoEmployee.TestDelete;
const
  cLastName = 'Smith';
  cEmployeeID = 3;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsEmployee.Delete;
  dmoEmployee.ApplyUpdates;
  DBValue := dmoTestHelper.GetDBValue('Employee', 'Count(1)', Format('EmployeeID=%d', [cEmployeeID]));
  Assert.AreEqual('0', DBValue, 'LastName');
end;

procedure TTestdmoEmployee.TestDeleteWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsProjectWork.Last;
  Assert.AreEqual(2, dmoEmployee.cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
  Assert.AreEqual(cProjectWorkID, dmoEmployee.cdsProjectWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  dmoEmployee.cdsProjectWork.Delete;
  dmoEmployee.ApplyUpdates;
  Assert.AreEqual<Int64>(0, dmoEmployee.cdsEmployee.ChangeCount, 'ChangeCount');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Count(1)', Format('ProjectWorkID=%d', [cProjectWorkID]));
  Assert.AreEqual('0', DBValue, 'Count');
end;

procedure TTestdmoEmployee.TestEdit;
const
  cLastName = 'B';
  cChangedName = 'NewLastName';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsEmployee.Edit;
  dmoEmployee.cdsEmployeeLASTNAME.AsString := cChangedName;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  Assert.AreEqual(1, dmoEmployee.cdsEmployeeUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('Employee', 'LastName', Format('EmployeeID=%d', [cEmployeeID]));
  Assert.AreEqual(cChangedName, DBValue, 'LastName');
end;

procedure TTestdmoEmployee.TestEditChangedEmployee;
const
  cLastName = 'B';
  cChangedName = 'NewLastName';
  cOtherUserChange = 'Other';
  cEmployeeID = 1;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoTestHelper.ChangeDBRecord('Employee', Format('FirstName=''%s'', UpdNo=UpdNo+1', [cOtherUserChange]), Format('EmployeeID=%d', [cEmployeeID]));
  dmoEmployee.OnApplyUpdateError := DoApplyUpdateError;
  FApplyUpdateErrorTriggered := False;
  dmoEmployee.cdsEmployee.Edit;
  dmoEmployee.cdsEmployeeLASTNAME.AsString := cChangedName;
  dmoEmployee.cdsEmployee.Post;
  dmoEmployee.ApplyUpdates;
  Assert.IsTrue(FApplyUpdateErrorTriggered, 'FApplyUpdateErrorTriggered');
  Assert.IsFalse(dmoEmployee.HasPendingChanges, 'HasPendingChanges');
  Assert.AreEqual(cOtherUserChange, dmoEmployee.cdsEmployeeFIRSTNAME.AsString, 'FirstName');
  Assert.AreEqual(1, dmoEmployee.cdsEmployeeUPDNO.AsInteger, 'UpdNo');
end;

procedure TTestdmoEmployee.TestEditWork;
const
  cLastName = 'B';
  cEmployeeID = 1;
  cProjectWorkID = 2;
  cHours = 25.0;
var
  DBValue: string;
begin
  RunCreateFixtureAtTearDown := True;
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(cEmployeeID, dmoEmployee.cdsEmployeeEMPLOYEEID.AsInteger, 'EmployeeID');
  dmoEmployee.cdsProjectWork.Last;
  Assert.AreEqual(2, dmoEmployee.cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
  Assert.AreEqual(cProjectWorkID, dmoEmployee.cdsProjectWorkPROJECTWORKID.AsInteger, 'ProjectWorkID');
  dmoEmployee.cdsProjectWork.Edit;
  dmoEmployee.cdsProjectWorkENDDATE.AsDateTime := Date;
  dmoEmployee.cdsProjectWorkHOURS.AsFloat := cHours;
  dmoEmployee.cdsProjectWork.Post;
  dmoEmployee.ApplyUpdates;
  Assert.AreEqual<Int64>(0, dmoEmployee.cdsEmployee.ChangeCount, 'ChangeCount');
  Assert.AreEqual(1, dmoEmployee.cdsProjectWorkUPDNO.AsInteger, 'UpdNo');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'EndDate', Format('ProjectWorkID=%d', [cProjectWorkID]));
  Assert.AreEqual(DateToStr(Date), DBValue, 'EndDate');
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'Hours', Format('ProjectWorkID=%d', [cProjectWorkID]));
  Assert.AreEqual(FloatToStr(cHours), DBValue, 'Hours');
end;

procedure TTestdmoEmployee.TestExportWorkingHoursToCSV;
const
  caCSV: array[0..1] of string =
    ('EMPLOYEEID,FIRSTNAME,LASTNAME,PROJECT,STARTDATE,ENDDATE,HOURS',
     '1,"Joe","Bloggs","Project A",14/07/2014,18/07/2014,40');
var
  DBValue: string;
  stlCSV: TStringList;
begin
  RunCreateFixtureAtTearDown := True;
  DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProcessedStmp', 'ProjectWorkID=1');
  Assert.AreEqual(EmptyStr, DBValue, 'ProcessedStmp');
  stlCSV := TStringList.Create;
  try
    stlCSV.Text := dmoEmployee.ExportWorkingHoursToCSV;
    DBValue := dmoTestHelper.GetDBValue('ProjectWork', 'ProcessedStmp', 'ProjectWorkID=1');
    Assert.IsTrue(DBValue <> EmptyStr, 'ProcessedStmp Is Not Null');
    Assert.AreEqual(2, stlCSV.Count, 'CSV.Count');
    Assert.AreEqual(caCSV[0], stlCSV[0], 'stlCSV[0]');
    Assert.AreEqual(caCSV[1], stlCSV[1], 'stlCSV[1]');
  finally
    stlCSV.Free;
  end;
end;

procedure TTestdmoEmployee.TestOpen;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  dmoEmployee.Open(cAnyValue, EmptyStr);
  Assert.AreEqual(3, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
end;

procedure TTestdmoEmployee.TestOpenWork;
const
  cLastName = 'B';
  cAnyValue = '%';
begin
  dmoEmployee.Open(cLastName, EmptyStr);
  Assert.AreEqual(1, dmoEmployee.cdsEmployee.RecordCount, 'cdsEmployee.RecordCount');
  Assert.AreEqual(2, dmoEmployee.cdsProjectWork.RecordCount, 'cdsProjectWork.RecordCount');
end;

initialization
  TDUnitX.RegisterTestFixture(TTestdmoEmployee);
end.
