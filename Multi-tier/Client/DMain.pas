unit DMain;

interface

uses
  Data.DB,
  System.Classes,
  Data.DBXDataSnap,
  Data.DBXCommon,
  Data.SqlExpr,
  IPPeerClient;

type
  TdmoMain = class(TDataModule)
    conAppServer: TSQLConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmoMain.DataModuleCreate(Sender: TObject);
begin
  conAppServer.Open;
end;

end.
